package persistence

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"gitlab.com/carleeto/lq/os_utils"
	"github.com/steveyen/gkvlite"
)

type KeyValStore struct {
	err    error
	f      *os.File
	s      *gkvlite.Store
	c      *gkvlite.Collection
	m      map[string]string
	name   string
	writer io.Writer
}

func (k *KeyValStore) make_sure_collection_exists() *gkvlite.Collection {
	if k.s != nil {
		var collection *gkvlite.Collection
		if collection = k.s.GetCollection(k.name); collection == nil {
			collection = k.s.SetCollection(k.name, nil)
		}
		collection = k.s.GetCollection(k.name)
		return collection
	}
	return nil
}

func (k KeyValStore) Keys() (keys []string) {
	for k, _ := range k.m {
		keys = append(keys, k)
	}
	return keys
}

func (k KeyValStore) KeyValMap() (key_val_map map[string]string) {
	return k.m
}

func (k KeyValStore) Error() error {
	return k.err
}

func (k *KeyValStore) Close() {
	k.err = k.s.Flush()
	if k.err != nil {
		fmt.Println(k.err)
	}
	if k.err = k.f.Sync(); k.err != nil {
		fmt.Println(k.err)
	}
	if k.err = k.f.Close(); k.err != nil {
		fmt.Println(k.err)
	}
}

func (k *KeyValStore) Reset() {
	if k.s != nil {
		k.s.RemoveCollection(k.name)
		k.s.SetCollection(k.name, nil)
	}
}

func (k *KeyValStore) Add(key, val string) {
	if collection := k.make_sure_collection_exists(); collection != nil {
		collection.Set([]byte(key), []byte(val))
		k.GetAll()
	}
}

func (k *KeyValStore) Remove(key string) (wasdeleted bool, err error) {
	if collection := k.make_sure_collection_exists(); collection != nil {
		defer k.GetAll()
		return collection.Delete([]byte(key))
	} else {
		fmt.Println("Collection does not exist")
	}
	return false, err
}

func (k *KeyValStore) DisplayAll() {
	f := func(i *gkvlite.Item) bool {
		return print_stored_val(i, k.writer)
	}
	if collection := k.make_sure_collection_exists(); collection != nil {
		iterate(collection, f)
	}
}

func (k *KeyValStore) GetAll() {
	if collection := k.make_sure_collection_exists(); collection != nil {
		if k.m == nil {
			k.m = make(map[string]string)
		}
		get_a := func(i *gkvlite.Item) bool {
			var keybuf, valbuf bytes.Buffer
			keybuf.Write(i.Key)
			valbuf.Write(i.Val)
			key := keybuf.String()
			val := valbuf.String()
			k.m[key] = val
			return true
		}
		iterate(collection, get_a)
	}
}

func (k *KeyValStore) Get(key string) string {
	if collection := k.make_sure_collection_exists(); collection != nil {
		var val []byte
		val, k.err = collection.Get([]byte(key))
		return string(val[:len(val)])
	}
	return ""
}

func NewKeyValStore(name string, w io.Writer) *KeyValStore {
	kvs := new(KeyValStore)
	kvs.f, kvs.err = os.OpenFile(os_utils.ConcatenatePath(os_utils.TempDir(), KEYVAL_STORE), os.O_RDWR|os.O_CREATE, os.ModeExclusive)
	if kvs.err != nil {
		return kvs
	}
	kvs.s, kvs.err = gkvlite.NewStore(kvs.f)
	kvs.writer = w
	return kvs
}

func print_stored_val(i *gkvlite.Item, w io.Writer) bool {
	var keybuf, valbuf bytes.Buffer
	keybuf.Write(i.Key)
	valbuf.Write(i.Val)
	fmt.Fprintf(w, "%s='%s'\n", keybuf.String(), valbuf.String())
	return true
}

func iterate(c *gkvlite.Collection, f func(i *gkvlite.Item) bool) {
	c.VisitItemsAscend([]byte(""), true, f)
}
