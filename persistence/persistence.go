package persistence

import "io"

const (
	KEYVAL_STORE            = "log_query.gkvlite"
	FLAGS_COLLECTION_NAME   = "flags"
	MACRO_COLLECTION_NAME   = "macros"
	HISTORY_COLLECTION_NAME = "history"
)

type KVStore interface {
	Close()
	Reset()
	Add(key, val string)
	Remove(key string) (wasdeleted bool, err error)
	DisplayAll()
	GetAll()
	Get(key string) string
	Error() error
	KeyValMap() map[string]string
	Keys() []string
}

func GetStoreByName(name string, w io.Writer) KVStore {
	switch name {
	case "flags":
		return DefaultKeyValStore(w)
	case "macros":
		return MacroKeyValStore(w)
	case "history":
		return HistoryKeyValStore(w)
	}
	return nil
}

func DefaultKeyValStore(w io.Writer) KVStore {
	return initKVStore(FLAGS_COLLECTION_NAME, w)
}

func MacroKeyValStore(w io.Writer) KVStore {
	return initKVStore(MACRO_COLLECTION_NAME, w)
}

func HistoryKeyValStore(w io.Writer) KVStore {
	return initKVStore(HISTORY_COLLECTION_NAME, w)
}

func initKVStore(name string, w io.Writer) KVStore {
	k := NewKeyValStore(KEYVAL_STORE, w)
	k.name = name
	k.GetAll()
	return k
}

func ResetKVStore(fk func() KVStore) error {
	k := fk()
	if err := k.Error(); err != nil {
		return err
	}
	defer k.Close()
	k.Reset()
	return nil
}
