package sipinfo

import (
	"errors"
	"strings"

	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
)

type SipInfoReader interface {
	From() []byte
	To() []byte
	CallID() []byte
	Media() [][]byte
	Type() []byte
	UnparsedLines() [][]byte
}

type SipInfo interface {
	Parse([]byte) error
	SipInfoReader
}

type sipinfo struct {
	from           []byte
	to             []byte
	callid         []byte
	media          [][]byte
	msgtype        []byte
	unparsed_lines [][]byte
}

func NewSipInfo() SipInfo {
	return new(sipinfo)
}

func (s *sipinfo) Parse(content []byte) error {
	//This function uses RFC 3261
	//Sections 8.1.1, 11.1 and 13.2.1 used as a guide
	//to only display the mandatory SIP headers
	line_separator := func(r rune) bool {
		return r == '\n'
	}
	lines := strings.FieldsFunc(string(content), line_separator)
	s.unparsed_lines = [][]byte{}
	for _, line := range lines {
		lline := strings.ToLower(line)
		if parsing.StartsWith(lline, "to:") {
			s.to = str_utils.StringToByteSlice(get_sip_uri(lline[len("to: "):len(lline)]))
		} else if parsing.StartsWith(lline, "from:") {
			s.from = str_utils.StringToByteSlice(get_sip_uri(lline[len("from: "):len(lline)]))
		} else if parsing.StartsWith(lline, "call-id: ") {
			s.callid = str_utils.StringToByteSlice(lline[len("call-id: "):len(lline)])
		} else if parsing.StartsWith(lline, "m=") {
			s.media = append(s.media, str_utils.StringToByteSlice(lline[0:len(lline)]))
		} else if msgtype := sipmsg_type(lline); msgtype != "" {
			s.msgtype = str_utils.StringToByteSlice(msgtype)
		} else {
			s.unparsed_lines = append(s.unparsed_lines, str_utils.StringToByteSlice(line))
		}
	}
	if len(s.to) > 0 && len(s.from) > 0 && len(s.callid) > 0 && len(s.msgtype) > 0 {
		//no error
		return nil
	}
	return errors.New("Required fields not present")
}

func (s sipinfo) From() (from []byte) {
	return s.from
}

func (s sipinfo) To() (to []byte) {
	return s.to
}

func (s sipinfo) CallID() (callid []byte) {
	return s.callid
}

func (s sipinfo) Media() (media [][]byte) {
	return s.media
}

func (s sipinfo) Type() (msgtype []byte) {
	return s.msgtype
}

func (s sipinfo) UnparsedLines() (lines [][]byte) {
	return s.unparsed_lines
}
