package sipinfo

import (
	"strings"
)

func get_sip_uri(line string) (sip_uri string) {
	lookfor := []string{
		"sip:",
		"sips:",
	}
	for _, possible_uri := range lookfor {
		if start := strings.Index(line, possible_uri); start >= 0 {
			sip_uri = strings.Replace(line, "<", "", -1)
			sip_uri = strings.Replace(sip_uri, ">", "", -1)
			sip_uri = strings.Replace(sip_uri, possible_uri, "", 1)
			if semicolon_index := strings.Index(sip_uri, ";"); semicolon_index > 0 {
				sip_uri = sip_uri[0:semicolon_index]
			}
			return strings.Trim(sip_uri, " \n\r")
		}
	}
	return ""
}

func siprequest_type(lowercase_line string) (request string) {
	lookfor := []string{
		"sip:",
		"sips:",
	}
	for _, possible_uri := range lookfor {
		for _, msgtype := range sip_requests() {
			str := msgtype + " " + possible_uri
			if start := strings.Index(strings.ToLower(lowercase_line), str); start >= 0 {
				return lowercase_line[start : start+len(msgtype)]
			}
		}
	}
	return request
}

func sipresponse_type(lowercase_line string) (response string) {
	for _, entry := range sip_responses() {
		code := entry[:2]
		explanation := entry[4:]
		prefix := "sip/2.0 "
		str := prefix + code
		if start := strings.Index(lowercase_line, str); start >= 0 {
			index := start + len(prefix)
			code := lowercase_line[index : index+3]
			return code + " " + explanation
		}
	}
	return response
}

func sipmsg_type(lowercase_line string) (msgtype string) {
	request := siprequest_type(lowercase_line)
	response := sipresponse_type(lowercase_line)
	if request != "" && response == "" {
		return request
	} else if request == "" && response != "" {
		return response
	} else if request == "" && response == "" {
		return ""
	}
	return "ERROR: Line is both request and response!" + lowercase_line
}

func sip_requests() []string {
	return []string{
		//requests:
		// taken from: http://en.wikipedia.org/wiki/List_of_SIP_request_methods
		"ack",
		"bye",
		"cancel",
		"info",
		"invite",
		"message",
		"notify",
		"options",
		"prack",
		"publish",
		"refer",
		"register",
		"subscribe",
		"update",
	}
}

func sip_responses() []string {
	return []string{
		//sip response codes.
		//taken from http://en.wikipedia.org/wiki/SIP_responses
		"100 Trying",
		"180 Ringing",
		"181 Call is Being Forwarded",
		"182 Queued",
		"183 Session in Progress",
		"199 Early Dialog Terminated",
		"200 OK",
		"202 Accepted",
		"204 No Notification",
		"300 Multiple Choices",
		"301 Moved Permanently",
		"302 Moved Temporarily",
		"305 Use Proxy",
		"380 Alternative Service",
		"400 Bad Request",
		"401 Unauthorized",
		"402 Payment Required",
		"403 Forbidden",
		"404 Not Found",
		"405 Method Not Allowed",
		"406 Not Acceptable",
		"407 Proxy Authentication Required",
		"408 Request Timeout",
		"409 Conflict",
		"410 Gone",
		"411 Length Required",
		"412 Conditional Request Failed",
		"413 Request Entity Too Large",
		"414 Request-URI Too Long",
		"415 Unsupported Media Type",
		"416 Unsupported URI Scheme",
		"417 Unknown Resource-Priority",
		"420 Bad Extension",
		"421 Extension Required",
		"422 Session Interval Too Small",
		"423 Interval Too Brief",
		"424 Bad Location Information",
		"428 Use Identity Header",
		"429 Provide Referrer Identity",
		"430 Flow Failed",
		"433 Anonymity Disallowed",
		"436 Bad Identity-Info",
		"437 Unsupported Certificate",
		"438 Invalid Identity Header",
		"439 First Hop Lacks Outbound Support",
		"470 Consent Needed",
		"480 Temporarily Unavailable",
		"481 Call/Transaction Does Not Exist",
		"482 Loop Detected.",
		"483 Too Many Hops",
		"484 Address Incomplete",
		"485 Ambiguous",
		"486 Busy Here",
		"487 Request Terminated",
		"488 Not Acceptable Here",
		"489 Bad Event",
		"491 Request Pending",
		"493 Undecipherable",
		"494 Security Agreement Required",
		"500 Server Internal Error",
		"501 Not Implemented",
		"502 Bad Gateway",
		"503 Service Unavailable",
		"504 Server Time-out",
		"505 Version Not Supported",
		"513 Message Too Large",
		"580 Precondition Failure",
		"600 Busy Everywhere",
		"603 Decline",
		"604 Does Not Exist Anywhere",
		"606 Not Acceptable",
	}
}
