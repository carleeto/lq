package os_utils

import (
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"runtime"

	"bufio"

	"github.com/carleeto/uniuri"
	"github.com/howeyc/gopass"
	"github.com/kardianos/osext"
)

func AppArgs() []string {
	return os.Args[1:]
}

func UserDir() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	return usr.HomeDir, nil
}

func UserHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}
		return home
	}
	return os.Getenv("HOME")
}

func AppDataDir(app string) string {
	if runtime.GOOS == "windows" {
		return os.Getenv("APPDATA") + "\\" + app
	}
	return os.Getenv("HOME") + "/." + app
}

func GetPasswordFromUser() ([]byte, error) {
	return gopass.GetPasswd()
}

func CurrentDirectory() (string, error) {
	return os.Getwd()
}

func env_variable(v string) string {
	return os.Getenv(v)
}

func GetFilesIn(d string) ([]os.FileInfo, error) {
	return ioutil.ReadDir(d)
}

func TempDir() string {
	return os.TempDir()
}

func random_string(length int) string {
	return uniuri.NewLen(length)
}

func RandomTempDir() string {
	return ConcatenatePath(TempDir(), random_string(5))
}

func random_temp_file() string {
	return ConcatenatePath(TempDir(), random_string(5))
}

func ConcatenatePath(p1, p2 string) string {
	return filepath.Join(p1, p2)
}

func FileExists(filename string) bool {
	if _, err := os.Stat(filename); err != nil {
		return os.IsExist(err)
	}
	return true
}

func CreatePath(path string) error {
	return os.MkdirAll(path, os.ModeDir)
}

func OpenWithDefaultApp(filename string) error {
	cmd := exec.Command("rundll32.exe", "url.dll,FileProtocolHandler", filename)
	err := cmd.Start()
	if err != nil {
		return err
	}
	return cmd.Wait()
}

func ExecuteShellCommand(command string) ([]byte, error) {
	var cmd *exec.Cmd
	cmd = exec.Command("cmd.exe", "/C", command)
	return cmd.CombinedOutput()
}

func ExecuteCommand(command string, args ...string) ([]byte, error) {
	var cmd *exec.Cmd
	cmd = exec.Command(command, args...)
	return cmd.CombinedOutput()
}

func BaseFilename(path string) string {
	return filepath.Base(path)
}

func FileExtension(path_to_file string) string {
	return filepath.Ext(path_to_file)
}

func PathToThisExecutable() string {
	s, _ := osext.Executable()
	return s
}

func CreateFile(filename string) (io.WriteCloser, error) {
	if dir := path.Dir(filename); !FileExists(dir) {
		CreatePath(dir)
	}
	return OpenFile(filename)
}

func OpenFile(filename string) (io.WriteCloser, error) {
	return os.Create(filename)
}

func CopyFile(from, to string) error {
	// open input file
	fi, err := os.Open(from)
	if err != nil {
		return err
	}
	// close fi on exit and check for its returned error
	defer func() {
		err = fi.Close()
	}()

	// open output file
	fo, err := os.Create(to)
	if err != nil {
		return err
	}
	// close fo on exit and check for its returned error
	defer func() {
		err = fo.Close()
	}()

	// make a buffer to keep chunks that are read
	buf := make([]byte, 1024)
	for {
		// read a chunk
		n, err := fi.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		// write a chunk
		if _, err := fo.Write(buf[:n]); err != nil {
			return err
		}
	}
	return nil
}

func WaitForEnterKey() chan bool {
	out := make(chan bool)
	go func() {
		reader := bufio.NewReader(os.Stdin)
		reader.ReadString('\n')
		out <- true
		close(out)
	}()
	return out
}

func WaitForKeypress() (string, error) {
	reader := bufio.NewReader(os.Stdin)
	return reader.ReadString('\n')
}
