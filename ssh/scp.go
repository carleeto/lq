package ssh

import (
	"fmt"
	"io"

	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"golang.org/x/crypto/ssh"
)

type password string

func (p password) Password() (string, error) {
	return string(p), nil
}

func DoScp(host, username, passwd, remote_file, local_file string) error {
	return scp_get(host, "22", username, passwd, remote_file, local_file, true /*log copy*/)
}

type interactive_authenticator struct{}

func print_if_not_empty(s string, add_newline bool) {
	if s != "" {
		fmt.Printf("%s ", s)
		if add_newline {
			fmt.Printf("\n")
		}
	}
}

func (f interactive_authenticator) Challenge(user, instruction string, questions []string, echos []bool) (answers []string, err error) {
	print_if_not_empty(instruction, true)
	print_if_not_empty(user, true)
	for _, q := range questions {
		print_if_not_empty(q, false)
		pass, err := os_utils.GetPasswordFromUser()
		if err != nil {
			return answers, err
		}
		answers = append(answers, string(pass))
		print_if_not_empty("\n", false)
	}
	return answers, nil
}

func scp_getfile(session *ssh.Session, local_file, remote_file string, report func(bytes_written int64)) error {
	// Now, the way we copy the remote file is to cat it over ssh and then
	// pipe the contents of stdout on the remote end directly into the local file
	go func() {
		r, _ := session.StdoutPipe()
		if w, err := os_utils.OpenFile(local_file); err == nil {
			defer w.Close()
			written, _ := io.Copy(w, r)
			report(written)
		}
	}()
	if err := session.Run("`which cat` " + remote_file); err != nil {
		println("Could not retrieve file. Reason:" + err.Error())
		return err
	}
	return nil
}

func scp_get(host, port, username, passwd, remote_file, local_file string, log bool) error {
	// Dial code is taken from the ssh package example
	var i interactive_authenticator
	p := password(passwd)
	config := &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.PasswordCallback(p.Password),
			ssh.KeyboardInteractive(i.Challenge),
		},
	}
	client, err := ssh.Dial("tcp", host+":"+port, config)
	if err != nil {
		println("Unable to connect: " + err.Error())
		return err
	}

	session, err := client.NewSession()
	if err != nil {
		println("Failed to create session: " + err.Error())
		return err
	}
	defer session.Close()
	r := func(bytes_written int64) {
		if log {
			from := host
			if port != "22" {
				from += ":" + port
			}
			println(from + ":" + remote_file + " -> " + local_file)
			println("(" + parsing.FileSizeToString(uint64(bytes_written)) + ")")
		}
	}
	scp_getfile(session, local_file, remote_file, r)

	return nil
}
