package regex

import (
	"testing"
)

func testline() string {
	return "17:00:08.911             : {10000} : [611¤¤SERVERMANAGER¤NZVOIP04¤SR¤®SE¤OK [sent:6, queued:0, discarded:0]¤®¶]                   : sent to {10080}"
}

func expected_data() string {
	line := testline()
	return line[13:len(line)]
}

func TestRegularExpressionMatchesSingleLine(t *testing.T) {
	input := "11:26:38.365 [#47641]  *INFO: Created media session manager [0x7fb684007090]"
	if RegexMatchesAnyLine(input, "session") == false {
		t.Fatal("Regex does not match")
	}
}

func TestRegularExpressionMatchesMultipleLines(t *testing.T) {
	input := "media session manager"
	if RegexMatchesAnyLine(input, "session") == false {
		t.Fatal("Regex does not match")
	}
}
