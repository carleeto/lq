package regex

import (
	"bytes"
	"regexp"

	"gitlab.com/carleeto/lq/str_utils"
)

func RegexMatchesAnyLine(s, regex string) bool {
	var valid_regex = regexp.MustCompile(regex)
	lines := bytes.Replace(str_utils.StringToByteSlice(s), str_utils.StringToByteSlice("\r\n"), str_utils.StringToByteSlice(" "), -1)
	return valid_regex.MatchString(string(lines))
}
