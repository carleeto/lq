package tree

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"testing"
)

type node struct {
	V int
}

func (n node) Sequence() int {
	return n.V
}

func initial_tree(val int) *Tree {
	return tree_from([]int{val})
}

func tree_from(val []int) *Tree {
	if len(val) == 0 {
		return nil
	}
	var tr *Tree
	tr = nil
	for _, v := range val {
		nd := new(node)
		nd.V = v
		tr = Insert(tr, nd)
	}
	return tr
}

func vals_from(t *Tree) (vals []int) {
	c := Walker(t)
	for val := range c {
		vals = append(vals, val.Sequence())
	}
	return vals
}

func slices_are_equal(a, b []int, unequal_length_msg, unequal_values_msg string) (are_equal bool) {
	if len(a) != len(b) {
		fmt.Printf(unequal_length_msg)
		return false
	}
	for i, v := range a {
		if v != b[i] {
			fmt.Printf(unequal_values_msg+"\n", a[i], b[i])
			return false
		}
	}
	return true
}

func serialize_tree(t Tree) (bytes.Buffer, error) {
	var buffer bytes.Buffer // Stand-in for the network.
	// Create an encoder and send a value.
	gob.Register(node{}) //because node implements the Node interface
	enc := gob.NewEncoder(&buffer)
	err := enc.Encode(t)
	return buffer, err
}

func deserialize_tree(b bytes.Buffer) (Tree, error) {
	dec := gob.NewDecoder(&b)
	var v Tree
	err := dec.Decode(&v)
	return v, err
}

func TestInsertingANode(t *testing.T) {
	if tr := initial_tree(1); tr == nil {
		t.Fatalf("Supposed to return a tree")
	}
}

func TestInsertedNodeIsValid(t *testing.T) {
	val := 1
	if tr := initial_tree(val); tr != nil {
		if v := tr.Data.Sequence(); v != val {
			t.Fatalf("Expected node value of %d. got %d", val, v)
		}
	}
}

func TestInsertingManyNodes(t *testing.T) {
	v := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	tr := tree_from(v)
	v2 := vals_from(tr)
	if !slices_are_equal(v, v2, "Wrong number of values from data", "Expected %d. Got %d") {
		t.Fail()
	}
}

func TestInsertingANodeInTheMiddle(t *testing.T) {
	vals := []int{0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100}
	tr := tree_from(vals)

	// insert a node.
	n := new(node)
	n.V = 53
	Insert(tr, n)
	v := vals_from(tr)

	//do we have what we expect?
	expected := []int{0, 10, 20, 30, 40, 50, 53, 60, 70, 80, 90, 100}
	if !slices_are_equal(v, expected, "Wrong number of values from data", "Expected %d. Got %d") {
		t.Fail()
	}
}

func TestSerialzing(t *testing.T) {
	vals := []int{10, 20, 30, 40, 50, 11}
	expected_deserialized := []int{10, 11, 20, 30, 40, 50}
	network, err := serialize_tree(*tree_from(vals))
	var v Tree
	if err != nil {
		t.Fatal("encode:", err)
	}
	v, err = deserialize_tree(network)
	if err != nil {
		t.Fatal("decode:", err)
	}
	if !slices_are_equal(vals_from(&v), expected_deserialized,
		"Wrong number of values from data",
		"Expected %d. Got %d") {
		t.Fail()
	}
}
