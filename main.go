// testlogentry project main.go
package main

import (
	"io"
	"os"

	"gitlab.com/carleeto/lq/console"
	"gitlab.com/carleeto/lq/execute"
	"gitlab.com/carleeto/lq/options"
	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/persistence"
)

/*
Assumptions:
------------

An entry in a log file: <timestamp> [contexts] <data>
1) always contains timestamps.
2) May contain zero or more contexts immediately following the timestamps (fields enclosed by []).
3) Contains data immediately following the contexts.
*/

func process_args(args []string, w io.Writer, r io.Reader) {
	if o, process, run_console, err := options.Interpret(args, w); err == nil {
		if run_console {
			console.StartConsole(func() persistence.KVStore { return persistence.HistoryKeyValStore(w) }, r, w, true)
		} else if process {
			execute.Process(o, w)
		}
	}
}

func main() {
	args := os_utils.AppArgs()
	process_args(args, os.Stderr, os.Stdin)
}
