module gitlab.com/carleeto/lq

go 1.14

require (
	github.com/carleeto/go-humanize v0.0.0-20131114071131-152bd44907e2
	github.com/carleeto/uniuri v0.0.0-20131223082149-bc4af7603a3e
	github.com/howeyc/gopass v0.0.0-20190910152052-7cb4b85ec19c
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/lxn/walk v0.0.0-20191128110447-55ccb3a9f5c1
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	github.com/steveyen/gkvlite v0.0.0-20141117050110-5b47ed6d7458
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	gopkg.in/Knetic/govaluate.v3 v3.0.0 // indirect
)
