// +build !windows

package clipboard

func CopyToClipboard(s string) {
	println("Clipboard on platforms other than Windows is not supported")
}
