// +build windows

package clipboard

import (
	"fmt"

	"github.com/lxn/walk"
)

func CopyToClipboard(s string) {
	if err := walk.Clipboard().SetText(s); err != nil {
		fmt.Println("Could not copy to clipboard:", err)
	}
}
