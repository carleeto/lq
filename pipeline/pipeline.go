package pipeline

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/carleeto/lq/fileio"
	"gitlab.com/carleeto/lq/mobility"
	"gitlab.com/carleeto/lq/options"
	"gitlab.com/carleeto/lq/regex"
	"gitlab.com/carleeto/lq/renderer"
	"gitlab.com/carleeto/lq/timedval"
)

func apply_filters(f []string, c <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	if len(f) == 0 {
		return c
	}
	cin := c
	for _, s := range f {
		c_ := RegexFilter(s, cin)
		cin = c_
	}
	return cin
}

func apply_rewriters(o options.Options, c <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	cin := c
	f := get_rewriters(o)
	if len(f) == 0 {
		return c
	}
	for _, rewrite_func := range f {
		c_ := Rewrite(rewrite_func, cin)
		cin = c_
	}
	return cin
}

func str2slice(s string) []string {
	return []string{s}
}

func get_regex(s string, param string) (regexs []string) {
	switch s {
	case "sip":
		return str2slice(`(?i)sip/2`)
	case "call_id":
		return str2slice(`(?i)call-id\:\s` + param)
	case "sipnum":
		return str2slice(`(?i)sip\:` + param)
	case "from":
		return str2slice(`(?i)from:.*sips?\:` + param)
	case "to":
		return str2slice(`(?i)invite sips?\:` + param)
	case "regex":
		return str2slice(param)
	case "regexi":
		return str2slice(`(?i)` + param)
	case "sipmsg":
		regexs = append(regexs, `(?i)sip/2`, param)
	default:
		return regexs
	}
	return regexs
}

func get_regex_list(o options.Options) []string {
	s := []string{}
	if o.Sip_output {
		s = append(s, get_regex("sip", "")...)
	}
	if o.Call_id != "" {
		s = append(s, get_regex("call_id", o.Call_id)...)
	}
	if o.Sipnum != "" {
		s = append(s, get_regex("sipnum", o.Sipnum)...)
	}
	if o.Sipmsg != "" {
		s = append(s, "(?i)sip/2")
		s = append(s, o.Sipmsg)
	}
	if o.From != "" {
		s = append(s, get_regex("from", o.From)...)
	}
	if o.To != "" {
		s = append(s, get_regex("to", o.To)...)
	}
	if o.Regex != "" {
		s = append(s, get_regex("regex", o.Regex)...)
	}
	if o.Regexi != "" {
		s = append(s, get_regex("regexi", o.Regexi)...)
	}
	return s
}

func get_rewriters(o options.Options) (rewriters []func(timedval.TimedVal) timedval.TimedVal) {
	if o.Summary {
		rewriters = append(rewriters, sip_summary)
	}
	if o.Call_flow {
		rewriters = append(rewriters, call_flow_rewriter)
	}
	return rewriters
}

type MyTimeFilter struct {
	after_time time.Time
	d          time.Duration
}

func (t *MyTimeFilter) Show(t1 timedval.TimedVal) bool {
	var zero_time time.Time
	tin := t1.GetTimestamp() //this should already have the date and time
	if !tin.Equal(zero_time) {
		if t.after_time.Equal(zero_time) {
			t_new := time.Date( //zero out the minutes, seconds and milliseconds
				tin.Year(), tin.Month(), tin.Day(), tin.Hour(),
				0, 0, 0,
				tin.Location())
			t.after_time = t_new.Add(t.d) //add in the duration ex. after 20m
		}
		return tin.After(t.after_time)
	}
	return false
}

func (t *MyTimeFilter) Done() {}

type RegexCounter struct {
	count  int
	regex  string
	writer io.Writer
}

func (r *RegexCounter) Show(t timedval.TimedVal) bool {
	if regex.RegexMatchesAnyLine(string(t.GetData()), r.regex) {
		r.count++
	}
	return true
}

func (r *RegexCounter) Done() {
	if r.regex != "" {
		fmt.Fprintf(r.writer, "Found [%d] occurences of [%s]\n", r.count, r.regex)
	}
}

func get_time_filter(o options.Options) Matcher {
	tf := new(MyTimeFilter)
	tf.d = o.After
	return tf
}

func render(c <-chan timedval.TimedVal, o options.Options) {
	r := renderer.GetRenderer(o)
	r.Begin()
	for e := range c {
		r.Render(e)
	}
	r.End()
}

type StartFilter struct {
	allow bool
	regex string
}

func (f *StartFilter) Show(t timedval.TimedVal) bool {
	if !f.allow {
		f.allow = regex.RegexMatchesAnyLine(string(t.GetData()), f.regex)
	}
	return f.allow
}

func (f StartFilter) Done() {}

func get_start_filter(o options.Options) Matcher {
	start_filter := new(StartFilter)
	start_filter.regex = o.Start
	return start_filter
}

func get_regex_counter(o options.Options, w io.Writer) Matcher {
	if o.Count == "" {
		return nil
	}
	r := new(RegexCounter)
	r.regex = o.Count
	r.writer = w
	return r
}

type StopFilter struct {
	allow bool
	regex string
}

func (f *StopFilter) Show(t timedval.TimedVal) bool {
	if f.regex != "" && regex.RegexMatchesAnyLine(string(t.GetData()), f.regex) {
		f.allow = false
	}
	return f.allow
}

func (f *StopFilter) Done() {}

func get_stop_filter(o options.Options) Matcher {
	stop_filter := new(StopFilter)
	stop_filter.allow = true
	stop_filter.regex = o.Stop
	return stop_filter
}

func apply_merge_filter(o options.Options, m mobility.MergeState, cin <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	if !o.MergeContexts {
		return cin
	}
	return m.Merge(cin)
}

//returns the next line so that you can use the return value as a parameter
//in a new call to the function to read the next line in
func StartProcessing(o options.Options, w io.Writer) {
	var m mobility.MergeState
	cin, dout := fileio.Tail(o.File_in, true, w)
	c1 := timedval.ConvertLineToTimedVal(cin)
	c2 := timedval.AddDate(dout, c1)
	c2a := apply_merge_filter(o, m, c2)
	c3 := Filter(get_time_filter(o), c2a)
	c4 := ContextFilter(o.Context, c3)
	c5 := apply_filters(get_regex_list(o), c4)
	c6 := apply_rewriters(o, c5)
	c7 := Filter(get_start_filter(o), c6)
	c8 := Filter(get_stop_filter(o), c7)
	c8a := Filter(get_regex_counter(o, w), c8)
	c9 := timedval.TSAdjust(o.Adjust_time, c8a)
	output(c9, o, w)
	if o.Wait_for_keypress_on_exit {
		fmt.Fprintf(w, "Press a key to exit")
		reader := bufio.NewReader(os.Stdin)
		reader.ReadString('\n')
	}
}

func output(c <-chan timedval.TimedVal, o options.Options, w io.Writer) {
	render_opts := func(e timedval.TimedVal) timedval.TimedVal {
		e.ShowTimestamp(!o.No_timestamps)
		e.ShowContexts(!o.No_contexts)
		return e
	}
	if o.Dont_render == false {
		render(Rewrite(render_opts, c), o)
	} else {
		for _ = range c {
		}
		log.SetOutput(w)
		log.Printf("Done\n")
	}
}
