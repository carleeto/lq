package pipeline

import (
	"gitlab.com/carleeto/lq/regex"
	"gitlab.com/carleeto/lq/timedval"
)

func DontRewrite(e timedval.TimedVal) timedval.TimedVal {
	return e
}

func Rewrite(f func(timedval.TimedVal) timedval.TimedVal, c <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	cout := make(chan timedval.TimedVal)
	go func() {
		for e := range c {
			if e1 := f(e); e1 != nil {
				cout <- e1
			}
		}
		close(cout)
	}()
	return cout
}

func RegexFilter(rgex string, c <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	cout := make(chan timedval.TimedVal)
	go func() {
		defer close(cout)
		for e := range c {
			if e != nil {
				if !regex.RegexMatchesAnyLine(string(e.GetData()), rgex) {
					e.Hide()
				}
			}
			cout <- e
		}
	}()
	return cout
}
