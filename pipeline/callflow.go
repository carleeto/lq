package pipeline

import (
	"bytes"
	"strings"

	"gitlab.com/carleeto/lq/context"
	"gitlab.com/carleeto/lq/sipinfo"
	"gitlab.com/carleeto/lq/str_utils"
	"gitlab.com/carleeto/lq/timedval"
)

func ascii_art(s sipinfo.SipInfo) (output []byte) {
	if s != nil {
		prefix := str_utils.StringToByteSlice("\t")
		prefix = append(prefix, s.From()...)
		content := bytes.ToUpper(s.Type())
		art := "\n" + string(prefix) + "-[ " + string(content) + " ]->" + string(s.To())
		for _, media_line := range s.Media() {
			art += "\n\t"
			for i := 0; i < len(prefix); i++ {
				art += " "
			}
			art += "| " + string(media_line)
		}
		return str_utils.StringToByteSlice(art)
	}
	return output
}

func call_flow_rewriter(e timedval.TimedVal) timedval.TimedVal {
	if e != nil {
		str := string(e.GetData())
		s := sipinfo.NewSipInfo()
		if s.Parse(str_utils.StringToByteSlice(str)) == nil {
			l := context.NewContext(strings.TrimSpace(string(s.CallID())))
			e.SetContexts([]context.Context{l})
			e.SetData(ascii_art(s))
		} else {
			return nil
		}
	}
	return e
}
