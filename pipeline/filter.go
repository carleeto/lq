package pipeline

import (
	"gitlab.com/carleeto/lq/timedval"
)

type Matcher interface {
	Show(t timedval.TimedVal) bool
	Done()
}

func Filter(m Matcher, cin <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	if m == nil {
		return cin
	} else {
		cout := make(chan timedval.TimedVal)
		go func() {
			for e := range cin {
				if e != nil {
					if !m.Show(e) {
						e.Hide()
					}
				}
				cout <- e
			}
			m.Done()
			close(cout)
		}()
		return cout
	}
}
