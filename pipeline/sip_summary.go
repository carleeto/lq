package pipeline

import (
	"fmt"
	"strings"

	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
	"gitlab.com/carleeto/lq/timedval"
)

func sip_summary(e timedval.TimedVal) timedval.TimedVal {
	//This function uses RFC 3261
	//Sections 8.1.1, 11.1 and 13.2.1 used as a guide
	//to only display the mandatory SIP headers
	line_separator := func(r rune) bool {
		return r == '\n'
	}
	is_line_we_need := func(s string) bool {

		//the start of the SIP message may not be on a separate line
		if strings.Contains(strings.ToLower(s), "sip/2.0") {
			return true
		}
		direction_specifiers := []string{
			"send to",
			"recv from",
			"send over websockets",
			"recv over websockets",
			"sent to:",
			"received from:",
		}
		required_sip_headers := []string{
			"accept:",
			"to:",
			"from:",
			"cseq:",
			"call-id:",
			"max-forwards:",
			"via:",
			"allow:",
			"supported:",
			"m=",          //display the media line in SDP content
			"server:",     //display the server line (duplex, pbx, etc)
			"user-agent:"} //user agent field is useful too

		for _, h := range required_sip_headers {
			if parsing.StartsWith(strings.ToLower(s), h) {
				return true
			}
		}
		for _, h := range direction_specifiers {
			if strings.Contains(strings.ToLower(s), h) {
				return true
			}
		}
		return false
	}
	str := string(e.GetData())
	output := ""
	lines := strings.FieldsFunc(str, line_separator)
	for _, v := range lines {
		if is_line_we_need(v) {
			output += fmt.Sprintf("%s\n", v)
		}
	}
	if output != "" {
		e.SetData(str_utils.StringToByteSlice(output + "\n"))
		return e
	}
	return nil
}
