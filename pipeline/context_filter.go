package pipeline

import (
	"gitlab.com/carleeto/lq/context"
	"gitlab.com/carleeto/lq/timedval"
)

func ContextFilter(regex string, c <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	if regex == "" {
		return c
	}
	cout := make(chan timedval.TimedVal)
	go func() {
		for e := range c {
			if e != nil {
				if contexts := e.GetContexts(); len(contexts) > 0 {
					if !context.ContextMatches(regex, e.GetContexts()) {
						e.Hide()
					}
				}
			}
			cout <- e
		}
		close(cout)
	}()
	return cout
}
