package export

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

func ExportToJSON(item interface{}, filename string) error {
	if b, err := json.MarshalIndent(item, "", "  "); err == nil {
		return ioutil.WriteFile(filename, b, os.ModeExclusive)
	} else {
		return err
	}
}
