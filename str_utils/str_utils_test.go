package str_utils

import (
	"testing"
)

func testline() string {
	return "17:00:08.911             : {10000} : [611¤¤SERVERMANAGER¤NZVOIP04¤SR¤®SE¤OK [sent:6, queued:0, discarded:0]¤®¶]                   : sent to {10080}"
}

func TestFirstWord(t *testing.T) {
	line := testline()
	expected := "17:00:08.911"
	if r, _ := FirstWord(line); r != expected {
		t.Fatalf("Expected [%s]. Got [%s]", expected, r)
	}
}

func TestFirstWordLineStartsWithSpace(t *testing.T) {
	line := " " + testline()
	expected := "17:00:08.911"
	if r, _ := FirstWord(line); r != expected {
		t.Fatalf("Expected [%s]. Got [%s]", expected, r)
	}
}

func TestFirstWordLineStartsWithTab(t *testing.T) {
	line := "\t" + testline()
	expected := "17:00:08.911"
	if r, _ := FirstWord(line); r != expected {
		t.Fatalf("Expected [%s]. Got [%s]", expected, r)
	}
}

func TestFirstWordWithLineThatOnlyHasSpaces(t *testing.T) {
	line := " "
	expected := ""
	if r, _ := FirstWord(line); r != expected {
		t.Fatalf("Expected [%s]. Got [%s]", expected, r)
	}
}

func TestFirstWordWithLineThatOnlyHasTwoWords(t *testing.T) {
	line := "hello world"
	expected := "hello"
	if f, _ := FirstWord(line); f != "hello" {
		t.Fatalf("Expected [%s]. Got [%s]", expected, f)
	}
	if _, r := FirstWord(line); r != "world" {
		t.Fatalf("Expected [%s]. Got [%s]", expected, r)
	}
}
