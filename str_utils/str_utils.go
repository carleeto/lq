package str_utils

import (
	"bytes"
	"strings"
	"unicode"
)

func RemoveLeadingWhiteSpace(line string) string {
	return strings.TrimLeftFunc(line, unicode.IsSpace)
}

func FirstWord(line string) (first_word, remaining string) {
	start := 0
	found_non_space := false
	for i, c := range line {
		is_space := unicode.IsSpace(c)
		if !is_space && !found_non_space {
			start = i
			found_non_space = true
		} else if is_space && found_non_space {
			return line[start:i], line[i+1:]
		}
	}
	if !found_non_space {
		return "", line
	}
	return line, ""
}

func StringToByteSlice(s string) []byte {
	return []byte(s)
}

func AppendStrings(s1, s2 string, implementation int) string {
	switch implementation {
	case 1:
		return s1 + s2
	case 2:
		{
			var buffer bytes.Buffer
			buffer.WriteString(s1)
			buffer.WriteString(s2)
			return buffer.String()
		}
	case 3:
		{
			parts := []string{s1, s2}
			return strings.Join(parts, "")
		}
	case 4:
		{
			slice := StringToByteSlice(s1)
			data := StringToByteSlice(s2)
			l := len(slice)
			if l+len(data) > cap(slice) { // reallocate
				// Allocate double what's needed, for future growth.
				newSlice := make([]byte, (l+len(data))*2)
				// Copy data (could use bytes.Copy()).
				for i, c := range slice {
					newSlice[i] = c
				}
				slice = newSlice
			}
			slice = slice[0 : l+len(data)]
			for i, c := range data {
				slice[l+i] = c
			}
			return string(slice)
		}
	case 5:
		{
			return string(append([]byte(s1), []byte(s2)...))
		}
	default:
		return s1 + s2
	}
}
