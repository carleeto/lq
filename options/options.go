package options

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"gitlab.com/carleeto/lq/ipfxlog"
	"gitlab.com/carleeto/lq/lqurl"
	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/registry"
	"gitlab.com/carleeto/lq/ssh"
	"gitlab.com/carleeto/lq/url"
	"gitlab.com/carleeto/lq/version"
)

/*//change the ha
Docopt syntax so far:
IPFX Log Query.

Usage:
  log_query -h | --help
  log_query -v | --version
  log_query -c | --console
  log_query -i | --ipfxsc
  log_query -r | --register
  log_query (-s|--server)
  log_query [options]

Options:
  -h --help     Show this screen.
  -v --version  Show version.
  -c --console  Start interactive console.
  -s --server   Use specified server for IPFXSC
  -i --ipfxsc   Use default server for IPFXSC
  -r --register Register to handle lq:// URLs
  --sip         Print SIP messages only
  --summary     Print SIP message summaries only
  --sipnum <number>   Print SIP messages involving <number>
  --call_id <callid>  Print SIP messages matching <callid>
  --from <number>     Print SIP messages from <number>
  --to <number>       Print SIP messages to <number>
  --sipmsg <type>     Print SIP messages of <type> ex. REGISTER
  --context <c>       Print log entries from context <c>
  --time <adjustment> Change time stamps by <adjustment>
  --regex <reg_ex>    Match entries using regular expression
  --regexi <reg_ex>   Case insensitive regex matching
  --format <output>   Output format. console | text | html
*/

//change the options hash function too
type Options struct {
	Adjust_time               time.Duration
	After                     time.Duration
	Call_id                   string
	Call_flow                 bool
	Context                   string
	Count                     string
	Cpuprofile                string
	Dont_render               bool
	File_in                   string
	File_out                  string
	Format                    string
	From                      string
	MergeContexts             bool
	No_contexts               bool
	No_timestamps             bool
	Regex                     string
	Regexi                    string
	Sipmsg                    string
	Sipnum                    string
	Sip_output                bool
	Start                     string //start logging once this regex matches
	Stop                      string //stop logging once this regex matches
	Summary                   bool
	To                        string
	Wait_for_keypress_on_exit bool
}

func PrintExamples() {
	fmt.Fprintf(os.Stderr, "\nThe flags must always come before the file\n\n")
	fmt.Fprintf(os.Stderr, "----------------------------------Examples--------------------------------\n\n")
	fmt.Fprintf(os.Stderr, " To exclude the first 20 minutes of a log:\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -after 20m <log>\n")
	fmt.Fprintf(os.Stderr, " To get SIP entries from a Duplex/PBX log:\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -sip <log>\n")
	fmt.Fprintf(os.Stderr, " To get SIP entries for a specific number(ex.7777):\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -sipnum 7777 <log>\n")
	fmt.Fprintf(os.Stderr, " To get SIP entries from a specific number(ex.7777):\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -from 7777 <log>\n")
	fmt.Fprintf(os.Stderr, " To get SIP entries to a specific number(ex.7777):\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -to 7777 <log>\n")
	fmt.Fprintf(os.Stderr, " To get SIP entries for a specific call id(ex.AEBE-5AC7-F0FB-122A):\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -sip -call_id AEBE <log>\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -sip -call_id AEBE-5AC7-F0FB-122A <log>\n")
	fmt.Fprintf(os.Stderr, " To add 1.15 seconds to the timestamp of the log entries:\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -time 1.15s <log>\n")
	fmt.Fprintf(os.Stderr, " To search for a particular regular expression:\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -regex SIP/2 <log>\n")
	fmt.Fprintf(os.Stderr, " To search for a particular context:\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -context error <log>\n")
	fmt.Fprintf(os.Stderr, "   "+os.Args[0]+" -context \"(error|warning)\" <log>\n")
	fmt.Fprintf(os.Stderr, " To use IPFXSC to get the duplex log from nzvoip01:\n")
	fmt.Fprintf(os.Stderr, "   current log file : "+os.Args[0]+" -server nzvoip01 duplex -0 \n")
	fmt.Fprintf(os.Stderr, "   log file 1 hr ago: "+os.Args[0]+" -server nzvoip01 duplex -1 \n")
	fmt.Fprintf(os.Stderr, " To use IPFXSC to get all Duplex SIP messages from 2988 on nzvoip01:\n")
	fmt.Fprintf(os.Stderr, "   current log file : "+os.Args[0]+" -server nzvoip01 -from 2988 duplex -0 \n")
	fmt.Fprintf(os.Stderr, "   log file 1 hr ago: "+os.Args[0]+" -server nzvoip01 -from 2988 duplex -1 \n")
	fmt.Fprintf(os.Stderr, "--------------------------------------------------------------------------\n\n")
}

func Interpret(arguments []string, w io.Writer) (o Options, process bool, run_console bool, err error) {
	if lqurl.IsLqUrl(arguments) {
		arguments = lqurl.ParseLqUrl(arguments[0]) //there will only be one)
		fmt.Println(equivalent_command_line(arguments))
		o.Wait_for_keypress_on_exit = true
	}
	server := ""
	destdir := ""
	ipfxsc := false
	print_version := false
	register_lq := false
	f := flag.NewFlagSet("options", flag.ContinueOnError)

	//some functions to reduce typing
	rb := func(b *bool, name, description string) {
		f.BoolVar(b, name, false, description)
	}
	rs := func(s *string, name, description string) {
		f.StringVar(s, name, "", description)
	}
	rsd := func(s *string, name, default_, description string) {
		f.StringVar(s, name, default_, description)
	}
	rd := func(d *time.Duration, name, description string) {
		f.DurationVar(d, name, 0, description)
	}

	rb(&ipfxsc, "ipfxsc", "Use IPFXSC with default settings")
	rb(&run_console, "console", "Start in interactive mode")
	rb(&run_console, "c", "Start in interactive mode")
	rb(&print_version, "version", "Display version")
	rb(&register_lq, "register", "Register to handle lq:// URLs (will trigger a UAC prompt)")
	rs(&server, "server", "Use IPFXSC to get the log file from <server>")
	rs(&destdir, "destdir", "Retrieve log to this folder")

	rb(&o.Dont_render, "no_render", "Don't render the result")
	rb(&o.Summary, "summary", "Display summarised SIP messages")
	rb(&o.Call_flow, "callflow", "Just print one line call flows")
	rs(&o.Call_id, "call_id", "Call ID to look for")
	rs(&o.Context, "context", "Filter by context (error, info, etc)")
	rs(&o.Count, "count", "Count number of occurences of regex")
	rsd(&o.Format, "format", "txt", "Output to a file and open it. Supported formats: txt, html")
	rs(&o.File_out, "dest", "Render output to <file>. Only supported for text format")
	rb(&o.MergeContexts, "merge_contexts", "Merge consecutive contexts")
	rb(&o.No_contexts, "no_contexts", "Don't render contexts")
	rb(&o.No_timestamps, "no_timestamps", "Don't render timestamps")
	rs(&o.Regex, "regex", "Regular expression to look for")
	rs(&o.Regexi, "regexi", "Case insensitive regular expression to look for")
	rb(&o.Sip_output, "sip", "Only search sip messages")
	rs(&o.Sipnum, "sipnum", "SIP entries for this number")
	rs(&o.Sipmsg, "sipmsg", "The SIP message type (INVITE, REGISTER, etc)")
	rs(&o.Start, "start", "Start printing only once this regex matches")
	rs(&o.Stop, "stop", "Stop printing once this regex matches")
	rs(&o.From, "from", "Find SIP messages from")
	rs(&o.To, "to", "Find SIP messages to")
	rd(&o.Adjust_time, "time", "Amount of time to adjust the timestamp")
	rd(&o.After, "after", "Exclude this duration from the start of the log")
	rs(&o.Cpuprofile, "cpuprofile", "Write CPU profile to file")

	print_cmdsyntax := func() {
		fmt.Fprintf(w, "usage: %s [flags] (filename|ipfxsc parameters)\n", os.Args[0])
	}
	f.Usage = func() {
		print_cmdsyntax()
		fmt.Fprintf(w, "\nFlags (and their default values):\n")
		f.PrintDefaults()
	}
	if err := f.Parse(arguments); err != nil {
		fmt.Fprintln(w, err)
	}

	if !f.Parsed() {
		fmt.Fprintln(w, "Unable to parse command line flags")
	}

	if run_console {
		return o, false, true, nil
	}

	if register_lq {
		fmt.Fprintf(w, "Registering to handle lq:// URLs..")
		registry.RegisterLqHandler()
		fmt.Fprintf(w, "done\n")
		o, process = validate_options(o)
		return o, false, false, nil
	}

	if print_version {
		fmt.Fprintf(w, "%s\n", version.Version)
		return o, false, false, nil
	} else if f.NArg() == 0 {
		if len(f.Args()) == 0 && f.NFlag() > 0 {
			fmt.Fprintln(w, "Perhaps you forgot the filename?")
			print_cmdsyntax()
		} else {
			fmt.Fprintln(w, "Missing arguments")
			f.Usage()
		}
		return o, false, false, fmt.Errorf("Missing arguments")
	} else if server != "" || ipfxsc {
		var err error
		o.File_in, err = ipfxlog.GetIPFXLog(server, f.Args(), w, destdir)
		if err != nil {
			return o, false, false, err
		} else if o.File_in == "not found" {
			o.File_in = ""
			return o, false, false, fmt.Errorf("File not found")
		}
	} else {
		var err error
		o.File_in, err = get_filename(f.Args()[0], destdir)
		if err != nil {
			return o, false, false, fmt.Errorf("Unable to get file: %s", f.Args()[0])
		}
	}
	o, process = validate_options(o)
	return o, process, run_console, nil
}

func validate_options(o Options) (o_out Options, ok bool) {
	if o.File_in == "" {
		return o, false
	}
	if o.File_out == "" {
		o.File_out = o.File_in
	}
	return o, true
}

func equivalent_command_line(arguments []string) (command_line string) {
	command_line = os_utils.BaseFilename(os_utils.PathToThisExecutable()) + " "
	first_one := true
	for _, arg := range arguments {
		if !first_one {
			command_line += " "
		}
		first_one = false
		if strings.Contains(arg, " ") {
			command_line += "\""
		}
		command_line += arg
		if strings.Contains(arg, " ") {
			command_line += "\""
		}
	}
	return command_line
}

func get_filename(arg string, destdir string) (string, error) {
	if parsing.StartsWith(arg, "ssh://") {
		host, user, password, remote_file := url.ParseUrl(arg)
		local_file := os_utils.BaseFilename(remote_file)
		if destdir != "" {
			local_file = os_utils.ConcatenatePath(destdir, os_utils.BaseFilename(remote_file))
		}
		if err := ssh.DoScp(host, user, password, remote_file, local_file); err == nil {
			return local_file, nil
		} else {
			return local_file, err
		}
	}
	return arg, nil
}
