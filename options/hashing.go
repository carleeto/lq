package options

import (
	"crypto/sha1"
	"encoding/base32"
	"fmt"
	"hash"

	"gitlab.com/carleeto/lq/str_utils"
)

func base32_version(b []byte) string {
	return base32.StdEncoding.EncodeToString(b)
}

type hasher struct {
	h hash.Hash
}

func create_hasher() hasher {
	var h hasher
	h.h = sha1.New()
	return h
}

func (h *hasher) hash(b []byte) {
	h.h.Write(b)
}

func (h *hasher) final() []byte {
	return h.h.Sum(nil)
}

// returns the same random string for the same combination of input file
// and processing options
func HashFromOptions(o Options) string {
	h := create_hasher()
	s := fmt.Sprintf("%+v", o)
	h.hash(str_utils.StringToByteSlice(s))
	return base32_version(h.final())
}
