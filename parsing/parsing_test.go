package parsing

import (
	"testing"
)

func StrsEquals(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func testline() string {
	return "17:00:08.911             : {10000} : [611¤¤SERVERMANAGER¤NZVOIP04¤SR¤®SE¤OK [sent:6, queued:0, discarded:0]¤®¶]                   : sent to {10080}"
}

func expected_data() string {
	line := testline()
	return line[13:len(line)]
}

func TestEndOfTimeStamp(t *testing.T) {
	line := testline()
	r := EndOfTimestamp(line)
	if r != 12 {
		t.Fatalf("End of timestamp is %d\n", r)
	}
}

func TestLenParseToArgumentsNoArgs(t *testing.T) {
	if len(ParsetoArguments("hello")) != 1 {
		t.Fail()
	}
}

func TestLenParseToArguments(t *testing.T) {
	if len(ParsetoArguments("hello args")) != 2 {
		t.Fail()
	}
}

func TestLenParseToArgumentsQuotes(t *testing.T) {
	if len(ParsetoArguments("hello \"args\"")) != 2 {
		t.Errorf("Expected 2 args. Got %d\n", len(ParsetoArguments("hello \"args\"")))
		t.Fail()
	}
}

func TestLenParseToArgumentsQuotesWithSpaces(t *testing.T) {
	if len(ParsetoArguments("hello \"args agrs2\"")) != 2 {
		t.Fail()
	}
}

func TestValuesParseToArguments(t *testing.T) {
	args := []string{"hello", "args"}
	if !StrsEquals(ParsetoArguments("hello args"), args) {
		t.Fail()
	}
}

func TestValuesParseToArgumentsQuotesAtTheEnd(t *testing.T) {
	args := []string{"hello", "args"}
	parsed := ParsetoArguments("hello \"args\"")
	if !StrsEquals(parsed, args) {
		t.Errorf("Expected [hello,args]. Got [")
		for _, v := range parsed {
			t.Errorf("%s,", v)
		}
		t.Errorf("\n")
		t.Fail()
	}
}

func TestValuesParseToArgumentsQuotesWithSpaces(t *testing.T) {
	args := []string{"hello", "arg1a arg1b"}
	parsed := ParsetoArguments("hello  \"arg1a arg1b\"")
	if !StrsEquals(parsed, args) {
		t.Errorf("Expected [hello,arg1a arg1b]. Got [")
		for _, v := range parsed {
			t.Errorf("%s,", v)
		}
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestValuesParseToArgumentsTwoQuotes(t *testing.T) {
	args := []string{"hello", "arg1", "arg2"}
	parsed := ParsetoArguments("hello  \"arg1\" \"arg2\"")
	if !StrsEquals(parsed, args) {
		t.Errorf("Expected [hello,arg1,arg2]. Got [")
		for _, v := range parsed {
			t.Errorf("%s,", v)
		}
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestValuesParseToArgumentsTwoQuotesWithSpaces(t *testing.T) {
	args := []string{"hello", "arg1a arg1b", "arg2a arg2b"}
	parsed := ParsetoArguments("hello  \"arg1a arg1b\" \"arg2a arg2b\"")
	if !StrsEquals(parsed, args) {
		t.Errorf("Expected [hello,arg1a arg1b,arg2a arg2b]. Got [")
		for _, v := range parsed {
			t.Errorf("%s,", v)
		}
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestLenValuesParseToArgumentsEmptyString(t *testing.T) {
	parsed := ParsetoArguments("")
	if len(parsed) != 0 {
		t.Errorf("Expected [0]. Got [")
		t.Errorf("%d", len(parsed))
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestLenValuesParseToArgumentsUnixFileName(t *testing.T) {
	parsed := ParsetoArguments("/usr/local/etc/webrtc2sip/log.txt")
	if len(parsed) != 1 {
		t.Errorf("Expected [1]. Got [")
		t.Errorf("%d", len(parsed))
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestValuesParseToArgumentsUnixFileName(t *testing.T) {
	args := []string{"scp", "/usr/local/etc/webrtc2sip/log.txt"}
	parsed := ParsetoArguments("scp /usr/local/etc/webrtc2sip/log.txt")
	if !StrsEquals(parsed, args) {
		t.Errorf("Expected [scp,/usr/local/etc/webrtc2sip/log.txt]. Got [")
		for _, v := range parsed {
			t.Errorf("%s,", v)
		}
		t.Errorf("]\n")
		t.Fail()
	}
}

func TestDateFromFilename(t *testing.T) {
	ts := <-DateFromFilename("20130910_MsgServ3_17.log")
	if ts.String() != "2013-09-10 00:00:00 +0000 UTC" {
		t.Log(ts)
		t.Fatal("Could not parse date")
	}
}
