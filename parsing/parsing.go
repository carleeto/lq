package parsing

import (
	"bytes"
	"math"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/carleeto/lq/str_utils"
	"github.com/carleeto/go-humanize"
)

func ReplaceAll(s, replace, with string) string {
	return strings.Replace(s, replace, with, -1)
}

func EndsWith(s, substring string) bool {
	return strings.LastIndex(s, substring) == len(s)-1
}

func StartsWith(s, substring string) bool {
	return strings.Index(s, substring) == 0
}

func FileSizeToString(bytes uint64) string {
	return humanize.Bytes(bytes)
}

func ParsetoArguments(command string) (args []string) {
	if len(command) == 0 {
		return args
	}
	if !strings.Contains(command, "\"") {
		return strings.Split(command, " ")
	}
	//We have quotes in the string

	if num_quotes := strings.Count(command, "\""); num_quotes%2 == 0 {
		accumulate := ""
		for _, v := range strings.Split(command, " ") {
			if len(v) > 0 && v[0] == '"' {
				v := v[1:] //remove the first quote
				if strings.Index(v, "\"") == len(v)-1 {
					//quoted word without spaces in it. remove the last quote
					args = append(args, v[0:len(v)-1])
				} else {
					//still looking for the end quote
					accumulate = v
				}
			} else if accumulate != "" {
				accumulate += " " + v
				if v[len(v)-1] == '"' {
					//if it ends in a quote, remove the quote
					args = append(args, accumulate[0:len(accumulate)-1])
					accumulate = ""
				}
			} else if v != "" {
				args = append(args, v)
			}
		}
		return args
	}
	return []string{}
}

func LevensteinDistance(a, b string) int {
	var cost int
	d := make([][]int, len(a)+1)
	for i := 0; i < len(d); i++ {
		d[i] = make([]int, len(b)+1)
	}

	var min1, min2, min3 int

	for i := 0; i < len(d); i++ {
		d[i][0] = i
	}

	for i := 0; i < len(d[0]); i++ {
		d[0][i] = i
	}

	for i := 1; i < len(d); i++ {
		for j := 1; j < len(d[0]); j++ {
			if a[i-1] == b[j-1] {
				cost = 0
			} else {
				cost = 1
			}

			min1 = d[i-1][j] + 1
			min2 = d[i][j-1] + 1
			min3 = d[i-1][j-1] + cost
			d[i][j] = int(math.Min(math.Min(float64(min1), float64(min2)), float64(min3)))
		}
	}

	return d[len(a)][len(b)]
}

func GetTimestamp(line string) (t time.Time, format string) {
	if StartsWithTimestamp(line) {
		return LineToTimestamp(line)
	}
	return t, format
}

func LineToTimestamp(line string) (t time.Time, format string) {
	time_text := "                            "
	remaining := line
	if time_text, remaining = str_utils.FirstWord(line); time_text != "" {
		t, format := StringToTime(time_text)
		if format == "" {
			//this may be a mobility log
			date_text := time_text
			next_word, _ := str_utils.FirstWord(remaining)
			if next_word != "" {
				long_timestamp := date_text + " " + next_word
				t, format = StringToTime(string(long_timestamp))
			}
		}
		return t, format
	}
	var zerotime time.Time
	return zerotime, ""
}

func StartsWithTimestamp(line string) bool {
	_, format := LineToTimestamp(line)
	return format != ""
}

func EndOfTimestamp(line string) int {
	_, format := LineToTimestamp(line)
	return len(format)
}

func StringToTime(s string) (t time.Time, format string) {
	formats := []string{"15:04:05.000", "2006-01-02 15:04:05.000"}
	var err error
	for _, v := range formats {
		if t, err = time.Parse(v, s); err == nil {
			format = v
			return t, format
		}
	}
	return t, ""
}

func DateFromFilename(filename string) <-chan time.Time {
	t := make(chan time.Time)
	go func() {
		file := filepath.Base(filename)
		date := bytes.Split(str_utils.StringToByteSlice(file), str_utils.StringToByteSlice("_"))
		day := date[0]
		if day != nil {
			ti, _ := time.Parse("20060102", string(day))
			t <- ti
		} else {
			var zerotime time.Time
			t <- zerotime
		}
		close(t)
	}()
	return t
}
