package main

import (
	"io/ioutil"
	"os"
	"testing"
)

func BenchmarkPBXLog(_ *testing.B) {
	//usually takes about 11 seconds
	filename := "./benchmarks/data/pbx.log"
	process_args([]string{"--no_render", filename}, ioutil.Discard, os.Stdin)
}

func TestMustNotCrash(_ *testing.T) {
	filename := "./testlogs/webrtc2sip_now.log.txt"
	for i := 0; i < 10; i++ {
		process_args([]string{"-summary", "-no_render", filename}, ioutil.Discard, os.Stdin)
	}
}
