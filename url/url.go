package url

import "net/url"

func ParseUrl(url_in string) (host, username, password, path string) {
	if u, err := url.Parse(url_in); err == nil {
		if u.User != nil {
			passwd, _ := u.User.Password()
			return u.Host, u.User.Username(), passwd, u.Path
		}
		return u.Host, username, password, u.Path
	}
	return host, username, password, path
}
