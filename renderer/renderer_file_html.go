package renderer

import (
	"fmt"
	"html"
	"log"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/carleeto/lq/timedval"
)

const (
	META       = "<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>"
	BODY_STYLE = "<body style='font-family:Roboto; font-size:10'>"
)

type HTMLFile struct {
	file     *os.File
	err      error
	filename string
}

func (p *HTMLFile) Begin() {
	p.file, p.err = os.Create(p.filename)
	if p.err == nil {
		p.printHeader()
		p.stringToFile("\t" + BODY_STYLE + "\n")
	} else {
		log.Printf("ERROR: %s\n", p.err)
		log.Printf("Could not create %s", p.filename)
	}
}

func (p *HTMLFile) stringToFile(s string) {
	if p.file != nil && p.err == nil {
		var n int
		n, p.err = p.file.WriteString(s)
		if p.err != nil {
			log.Printf("ERROR: %s.\nWrote %d/%d bytes", p.err, n, len(s))
		}
	} else {
		if p.file == nil {
			log.Printf("ERROR: file is nil.\n")
		}
		if p.err != nil {
			log.Printf("ERROR: %s\n", p.err)
		}
	}
}

func (p *HTMLFile) End() {
	p.stringToFile("\t</body>\n</html>")
	p.file.Close()
	log.Printf("Starting\t%s\n", p.file.Name())
	cmd := exec.Command("cmd", "/C", "start", p.file.Name())
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	err = cmd.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

func (p *HTMLFile) Render(s timedval.TimedVal) {
	if !s.IsHidden() {
		str := s.String()
		str = html.EscapeString(str)
		str = strings.Replace(str, "\r", "", -1)
		str = strings.Replace(str, "\n", "<br>", -1)
		s2 := fmt.Sprintf("\t\t<div class=\"text-info\">%s</div>\n", str)
		p.stringToFile(s2)
	}
}

func (p *HTMLFile) printHeader() {
	p.stringToFile("<!DOCTYPE html>\n")
	p.stringToFile("<html>\n\t<head>\n\t\t" + META + "\n")
	p.printStyleSheet()
	p.stringToFile("\t</head>\n")
}

func (p *HTMLFile) printStyleSheet() {
	fonts := []string{
		"\t\t" + META + "\n",
	}
	for _, f := range fonts {
		p.stringToFile(f)
	}
}
