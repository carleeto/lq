package renderer

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/carleeto/lq/options"
	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
	"gitlab.com/carleeto/lq/timedval"
)

type Renderer interface {
	Begin()
	End()
	Render(s timedval.TimedVal)
}

func print_format_help(s string) {
	log.Printf("Unknown format %s\n", s)
	log.Printf("Only the following formats are recognised:\n")
	supported := []string{"text", "html", "console"}
	for _, v := range supported {
		log.Printf("\t%s\n", v)
	}
	log.Printf("Printing to the console instead.\n")
	time.Sleep(4000 * time.Millisecond) //Give them some time to read this before it scrolls away
}

func destfile_to_path(o options.Options) string {
	format_ext := "." + o.Format
	if o.File_out == "" {
		return os_utils.ConcatenatePath(os_utils.TempDir(), options.HashFromOptions(o)+format_ext)
	} else {
		if os_utils.FileExtension(o.File_out) == format_ext {
			return os_utils.ConcatenatePath(os_utils.TempDir(), options.HashFromOptions(o)+"_"+os_utils.BaseFilename(o.File_out))
		}
		return os_utils.ConcatenatePath(os_utils.TempDir(), os_utils.BaseFilename(o.File_out)) + format_ext
	}
}

func GetRenderer(o options.Options) Renderer {
	switch o.Format {
	case "txt":
		r := new(TextFile)
		r.filename = destfile_to_path(o)
		return r
	case "html":
		r := new(HTMLFile)
		r.filename = destfile_to_path(o)
		return r
	case "console":
		return new(PlainText)
	default:
		print_format_help(o.Format)
		return new(PlainText)
	}
}

type PlainText struct {
}

func (p PlainText) Render(s timedval.TimedVal) {
	if !s.IsHidden() {
		str := s.String()
		if !parsing.EndsWith(string(s.GetData()), "\n") {
			s.SetData(append(s.GetData(), str_utils.StringToByteSlice("\n")...))
		}
		fmt.Printf("%s", str)
	}
}
func (p PlainText) Begin() {
}
func (p PlainText) End() {
}
