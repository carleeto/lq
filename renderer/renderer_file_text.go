package renderer

import (
	"log"
	"os"

	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/timedval"
)

type TextFile struct {
	file     *os.File
	err      error
	filename string
}

func (p *TextFile) Begin() {
	p.file, p.err = os.Create(p.filename)
}

func (p *TextFile) stringToFile(s string) {
	if p.file != nil && p.err == nil {
		var n int
		if !parsing.EndsWith(s, "\n") {
			s = s + "\n"
		}
		n, p.err = p.file.WriteString(s)
		if p.err != nil {
			log.Printf("ERROR: %s.\nWrote %d/%d bytes", p.err, n, len(s))
		}
	} else {
		if p.file == nil {
			log.Printf("ERROR: file is nil.\n")
		}
		if p.err != nil {
			log.Printf("ERROR: %s\n", p.err)
		}
	}
}
func (p *TextFile) End() {
	name := p.file.Name()
	if s, err := p.file.Stat(); err == nil {
		if s.Size() > 0 {
			log.Printf("Starting\t%s\n", name)
			p.file.Close()
			os_utils.OpenWithDefaultApp(name)
		} else {
			log.Printf("No matches in %s\n", name)
		}
	} else {
		log.Println(err)
	}
}
func (p *TextFile) Render(s timedval.TimedVal) {
	if !s.IsHidden() {
		str := s.String()
		p.stringToFile(str)
		s.Release()
	}
}
