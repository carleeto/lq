package renderer

import (
	"testing"

	"gitlab.com/carleeto/lq/options"
)

func TestDestFileToPathWithEmptyOptionsProducesSomethingStupid(t *testing.T) {
	//var o options.Options
	//s := destfile_to_path(o)
	//println(s)
	/*Fix this*/
}

func TestDestFileToPathWithFormatSetButOutputFileNotSet(t *testing.T) {
	var o options.Options
	o.Format = "txt"
	s := destfile_to_path(o)
	extension := s[len(s)-(len(o.Format)+1):]
	if extension != "."+o.Format {
		println(extension)
		t.Fail()
	}
}

func TestDestFileToPathWithAndOutputFileSet(t *testing.T) {
	var o options.Options
	o.Format = "txt"
	o.File_out = "t.txt"
	s := destfile_to_path(o)
	filename := s[len(s)-len(o.File_out):]
	if filename != o.File_out {
		println(filename)
		t.Fail()
	}
}

func TestDestFileToPathWithAndOutputFileSetButOutputExtensionDifferent(t *testing.T) {
	var o options.Options
	o.Format = "txt"
	o.File_out = "t.tmp"
	expected := "t.tmp.txt"
	s := destfile_to_path(o)
	filename := s[len(s)-len(expected):]
	if filename != expected {
		println(s)
		t.Fail()
	}
}
