package fileio

import (
	"fmt"
	"io"
)

type nullRange struct {
	count        int64
	start_offset int64
	end_offset   int64
}

type LineScanner struct {
	line               []byte
	lines              [][]byte
	c                  chan string
	print_progress     bool
	line_count         int
	progress_threshold int
	writer             io.Writer
	continuous_nulls   nullRange
	bytecount          int64
}

func (w *LineScanner) Close() error {
	if len(w.line) > 0 {
		w.c <- string(w.line)
	}
	return nil
}

func (w *LineScanner) reset_continuous_null_count() {
	defer func() {
		w.continuous_nulls.count = 0
		w.continuous_nulls.start_offset = -1
		w.continuous_nulls.end_offset = -1
	}()
	if w.continuous_nulls.count > 0 {
		fmt.Fprintln(w.writer, w.continuous_nulls.count, "null chars found between [", w.continuous_nulls.start_offset, w.continuous_nulls.end_offset, "]")
	}
}

func (w *LineScanner) Write(p []byte) (n int, err error) {
	line := w.line
	defer func() { w.line = line }()
	for _, b := range p {
		switch b {

		case 0:
			//if we have a null, its probably goingto be followed by more
			//just count it and record the offset range
			if w.continuous_nulls.count == 0 {
				w.continuous_nulls.start_offset = w.bytecount
			} else {
				w.continuous_nulls.end_offset = w.bytecount
			}
			w.continuous_nulls.count++
		case '\r':
		default:
			//the ipfx message separators arent real utf8. transform them.
			switch b {
			case 0xa4:
				b = '¤'
			case 0xae:
				b = '®'
			case 0xbb:
				b = '»'
			case 0xb6:
				b = '¶'
			}

			//handle the normal case
			if w.continuous_nulls.count == 0 && b != '\n' {
				line = append(line, b)
			} else if w.continuous_nulls.count > 0 {
				w.reset_continuous_null_count()
			} else if b == '\n' {
				//end of line. send the scanned line out
				w.c <- string(append(line, []byte("\n")...))
				line = []byte{}
			}
		}
		w.bytecount++
	}
	return len(p), err
}

func (w *LineScanner) printProgress() {
	if w.print_progress {
		w.line_count++
		if w.line_count%w.progress_threshold == 0 {
			for i := 0; i < 30; i++ {
				fmt.Fprintf(w.writer, "\b")
			}
			fmt.Fprintf(w.writer, "%d lines processed", w.line_count)
		}
	}
}

func new_line_scanner(print_progress bool, progress_threshold int, c chan string, w io.Writer) *LineScanner {
	ls := new(LineScanner)
	ls.c = c
	ls.print_progress = print_progress
	ls.progress_threshold = progress_threshold
	ls.writer = w
	ls.continuous_nulls.count = 0
	ls.continuous_nulls.start_offset = -1
	ls.continuous_nulls.end_offset = -1
	return ls
}
