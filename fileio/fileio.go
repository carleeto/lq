package fileio

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/carleeto/lq/parsing"
)

// tail currently has a bug where it does not allow the file it is monitoring to
// be renamed or deleted. this is a problem when monitoring log files that are
// active near the end of the hour
func Tail(fi string, print_progress bool, w io.Writer) (<-chan string, <-chan time.Time) {
	c := make(chan string)
	go func() {
		defer close(c)
		log.SetOutput(w)
		log.Printf("Processing \t%s\n", fi)
		f, err := os.Open(fi)
		if err != nil {
			fmt.Fprintf(w, "error opening file %s: %v\n", fi, err)
			return
		} else {
			defer f.Close()
			scan_lines_2(f, print_progress, c, w)
		}
	}()
	return c, parsing.DateFromFilename(fi)
}

func scan_lines_2(f io.Reader, print_progress bool, c chan string, w io.Writer) {
	progress_threshold := 30000
	r := bufio.NewReader(f)
	ls := new_line_scanner(print_progress, progress_threshold, c, w)
	r.WriteTo(ls)
	if ls.print_progress && ls.line_count > ls.progress_threshold {
		fmt.Fprintf(w, "\r%d lines processed\n", ls.line_count)
	}
	ls.Close()
}
