package context

import (
	"testing"
)

func testline() string {
	return "17:00:08.911             : {10000} : [611¤¤SERVERMANAGER¤NZVOIP04¤SR¤®SE¤OK [sent:6, queued:0, discarded:0]¤®¶]                   : sent to {10080}"
}

func expected_data() string {
	line := testline()
	return line[13:len(line)]
}

func TestEmptyContexts(t *testing.T) {
	line := testline()
	c, _ := GetContext(line)
	if len(c) != 0 {
		t.Fatal("found sone contexts when none exist")
	}
}

func TestRemainingIsTheLineWithoutTheTimeStamp(t *testing.T) {
	line := testline()
	_, r := GetContext(line)
	looking_for := expected_data()
	if r != looking_for {
		t.Fatal("Remaining is \n[" + r + "]\nIt should be \n[" + looking_for + "]")
	}
}
