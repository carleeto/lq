package context

import (
	"bytes"
	"regexp"

	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
)

type Context interface {
	Name() string
	String() string
	Matches(rhs Context) bool
}

func ContextsAreEqual(lhs []Context, rhs []Context) bool {
	if len(lhs) == len(rhs) && len(lhs) > 0 {
		for i, c := range lhs {
			if c != nil && !c.Matches(rhs[i]) {
				return false
			}
		}
		return true
	}
	return false
}

type logcontext struct {
	name string
}

func (l logcontext) String() string {
	s := ""
	s = str_utils.AppendStrings(s, "[", 0)
	s = str_utils.AppendStrings(s, l.name, 0)
	s = str_utils.AppendStrings(s, "] ", 0)
	return s
}

func (l logcontext) Name() string {
	return l.name
}

func (l logcontext) Matches(rhs Context) bool {
	if rhs != nil {
		return l.String() == rhs.String()
	}
	return false
}

func NewContext(s string) Context {
	l := new(logcontext)
	l.name = s
	return l
}

func GetContext(line string) (context []Context, remaining string) {
	eot := parsing.EndOfTimestamp(line)
	if len(line) <= eot {
		return context, remaining
	}
	context_offset := parsing.EndOfTimestamp(line) + 1
	if i := bytes.Index(str_utils.StringToByteSlice(line), str_utils.StringToByteSlice("[")); i == context_offset {
		if j := bytes.Index(str_utils.StringToByteSlice(line), str_utils.StringToByteSlice("]")); j > i && j < len(line) {
			context = append(context, NewContext(line[i+1:j]))
			if remaining = line[j+1 : len(line)]; len(remaining) > 0 {
				for remaining[0] == '[' {
					j := bytes.Index(str_utils.StringToByteSlice(remaining), str_utils.StringToByteSlice("]"))
					if j != -1 {
						context = append(context, NewContext(remaining[1:j]))
						if len(remaining) > j+1 {
							remaining = remaining[j+1 : len(remaining)]
						} else {
							break
						}
					} else {
						break
					}
				}
				return context, remaining
			}
		}
	}
	return context, line[context_offset:len(line)]
}

func ContextMatches(regex string, contexts []Context) bool {
	for _, con := range contexts {
		var valid_regex = regexp.MustCompile(regex)
		if valid_regex.MatchString(con.Name()) {
			return true
		}
	}
	return false
}
