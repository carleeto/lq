package ipfxlog

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"

	"gitlab.com/carleeto/lq/os_utils"
)

func retrieve_to_random_dir(server string, args []string, w io.Writer) (string, error) {
	dir := os_utils.RandomTempDir()
	args_long := []string{}
	if server != "" {
		args_long = append(args_long, "server")
		args_long = append(args_long, server)
		args_long = append(args_long, ";")
	}
	args_long = append(args_long, "logcopy")
	args_long = append(args_long, args...)
	args_long = append(args_long, dir)
	str := "Executing\tipfxsc "
	for _, v := range args_long {
		str += v + " "
	}
	log.Printf(str)
	c := exec.Command("ipfxsc.exe", args_long...)
	out, err := c.CombinedOutput()
	if err != nil {
		log.Printf("Unable to retrieve the log file using ipfxsc: %s\n", err)
		log.Printf("\n----------------------\nIPFXSC output follows:\n")
		fmt.Fprintf(w, "----------------------\n")
		fmt.Fprintf(w, "%s\n", out)
		return "", err
	}
	if dirlist, err := os_utils.GetFilesIn(dir); err == nil {
		if len(dirlist) > 0 {
			//because we just created this folder, there will only be one file in it
			return os_utils.ConcatenatePath(dir, dirlist[0].Name()), nil
		} else {
			return "", fmt.Errorf("No log file was retrieved\n")
		}
	} else {
		return "", err
	}
}

func GetIPFXLog(server string, args []string, w io.Writer, dir string) (string, error) {
	if dir == "" {
		dir = os_utils.RandomTempDir()
	}

	if !os_utils.FileExists(dir) {
		os_utils.CreatePath(dir)
	}
	if file, err := retrieve_to_random_dir(server, args, w); err != nil {
		return file, err
	} else {
		dest := dir + string(os.PathSeparator) + os_utils.BaseFilename(file)
		//copy the file to the dest folder
		if err := os_utils.CopyFile(file, dest); err != nil {
			return "", err
		}
		return dest, nil
	}
}
