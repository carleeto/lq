package macros

import (
	"io"
	"gitlab.com/carleeto/lq/persistence"
)

func IsMacro(c string, w io.Writer) (macro_value string) {
	k := persistence.MacroKeyValStore(w)
	defer k.Close()
	return k.Get(c)
}
