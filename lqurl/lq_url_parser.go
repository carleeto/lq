package lqurl

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"gitlab.com/carleeto/lq/parsing"
)

func IsLqUrl(args []string) bool {
	return len(args) == 1 && parsing.StartsWith(args[0], "lq://")
}

func ParseLqUrl(url_ string) (arguments []string) {
	url_ = parsing.ReplaceAll(url_, "lq://", "")
	if parsing.EndsWith(url_, "/") {
		url_ = url_[0 : len(url_)-1]
	}
	if b, err := base64.StdEncoding.DecodeString(url_); err != nil {
		fmt.Println("error:", err)
		return arguments
	} else {
		url_ = string(b)
	}
	if err := json.Unmarshal([]byte(url_), &arguments); err != nil {
		fmt.Println("Error: ", err)
	}
	return arguments
}

func args_to_json_string(args []string) (s string) {
	if b, err := json.Marshal(args); err != nil {
		fmt.Println(err)
		return s
	} else {
		return string(b)
	}
}

func base64_version(s string) (b64s string) {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

func ArgsToUrl(args []string) (s string) {
	return "lq://" + base64_version(args_to_json_string(args))
}
