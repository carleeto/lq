package console

type ConsoleCommandMap struct {
	c []ConsoleCommand
}

func (ccm ConsoleCommandMap) Functions() []ConsoleCommand {
	return ccm.c
}

//Add a function here and it will be made available at the console
//Add its implementation in console_commands.go
func NewConsoleCommandMap(commands []ConsoleCommand) CommandMap {
	ccm := new(ConsoleCommandMap)
	ccm.c = commands
	return ccm
}

func console_command_map() []ConsoleCommand {
	m := []ConsoleCommand{
		NewCommand(do, "Process all files whose file names contain <substring>", []string{"do", "d"}),
		NewCommand(doall, "Process all files in the current directory", []string{"doall", "da"}),
		NewCommand(exec, "Execute <command> using stored flags as defaults", []string{"exec", "e"}),
		NewCommand(exprt, "Export stored flags, macros or history", []string{"export", "ex"}),
		NewCommand(get, "Get the value of a stored flag", []string{"get", "g"}),
		NewCommand(helpfunc, "Print valid commands", []string{"help", "?"}),
		NewCommand(history, "Print command history", []string{"history", "h"}),
		NewCommand(ls, "List files in current folder", []string{"ls"}),
		NewCommand(macro, "Create macro for command", []string{"macro", "m"}),
		NewCommand(print_version, "Print version", []string{"version", "v"}),
		NewCommand(reset, "Clear all stored flags", []string{"reset"}),
		NewCommand(run, "Run <command> without using stored flags", []string{"run", "r"}),
		NewCommand(scp, "Copy a file from a remote server using SCP", []string{"scp"}),
		NewCommand(script_console, "Define a sequence of actions", []string{"script", "sc"}),
		NewCommand(set, "Store a flag and its value", []string{"set", "s"}),
		NewCommand(sh, "Execute shell command", []string{"sh"}),
		NewCommand(share, "Print a URL that can be shared", []string{"share", "lq"}),
		NewCommand(stopfunc, "Exit the console", []string{"exit", "x", "q"}),
		NewCommand(unset, "Delete a stored flag or macro", []string{"unset", "u"})}
	return NewConsoleCommandMap(m).Functions()
}
