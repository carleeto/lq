package console

import (
	"io"
	"time"
)

type Command struct {
	f            func(string, io.Writer) ContinueOrNot
	usage_string string
	aliases      []string
	t            time.Duration
}

//command is a single string of all parameters
func (c *Command) Execute(command string, w io.Writer) ContinueOrNot {
	t := time.Now()
	retval := c.f(command, w)
	c.t = time.Since(t)
	return retval
}

func (c Command) TimeTakenForLastExecution() time.Duration {
	return c.t
}

func (c Command) Usage() string {
	return c.usage_string
}

func (c Command) Aliases() []string {
	return c.aliases
}

func NewCommand(f func(string, io.Writer) ContinueOrNot, usage string, aliases []string) ConsoleCommand {
	c := new(Command)
	c.f = f
	c.usage_string = usage
	c.aliases = aliases
	return c
}
