package console

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/carleeto/lq/fileio"
	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
	"gitlab.com/carleeto/lq/version"
)

const (
	script_extension = ".lqs.txt"
)

func scripting_command_map() []ConsoleCommand {
	m := []ConsoleCommand{
		NewCommand(script_edit, "Edit a script", []string{"edit", "e"}),
		NewCommand(script_run, "Run a script", []string{"run", "r"}),
		NewCommand(script_listall, "List all scripts", []string{"list", "l"}),
	}
	return NewConsoleCommandMap(m).Functions()
}

func script_dir() string {
	return os_utils.AppDataDir(version.AppName)
}

func scriptfile(scriptname string) string {
	filename := script_dir() + "\\" + scriptname + script_extension
	return filename
}

func script_edit(c string, w io.Writer) ContinueOrNot {
	params := parsing.ParsetoArguments(c)
	if len(params) < 1 {
		print_syntax_and_usage("edit", "<scriptname>"+"(without "+script_extension+")", scripting_command_map(), w)
		return CONTINUE
	}
	filename := scriptfile(params[0])
	//create if it doesn't exist
	if !os_utils.FileExists(filename) {
		f, _ := os_utils.CreateFile(filename)
		f.Write([]byte("//this is a comment\n"))
		f.Write([]byte("// ** make sure your file ends with a new line **\n"))
		f.Close()
	}
	if err := os_utils.OpenWithDefaultApp(filename); err != nil {
		fmt.Fprintln(w, err)
	}
	return CONTINUE
}

func script_run(c string, w io.Writer) ContinueOrNot {
	params := parsing.ParsetoArguments(c)
	if len(params) < 1 {
		print_syntax_and_usage("run", "<scriptname>", scripting_command_map(), w)
		fmt.Fprintln(w, "--------------------------------")
		script_listall("", w)
		fmt.Fprintln(w, "--------------------------------")
		return CONTINUE
	}
	filename := scriptfile(params[0])
	lines, date := fileio.Tail(filename, false, w)
	go func() { _ = <-date }()
	for line := range lines {
		//with scripts, carriage returns will only be at the end of the line
		line = strings.Replace(line, "\n", "", -1)
		line = str_utils.RemoveLeadingWhiteSpace(line)
		if len(line) > 1 && line[0:2] == "//" {
			continue //its a comment. Ignore it.
		} else if cont := process_input(line, console_command_map(), false, w); cont == STOP {
			fmt.Fprintln(w, "stopped")
			break
		}
	}
	return CONTINUE
}

func script_listall(_ string, w io.Writer) ContinueOrNot {
	fmt.Fprintf(w, "Scripts in %s:\n", script_dir())
	files, _ := os_utils.GetFilesIn(script_dir())
	for _, file := range files {
		fname := strings.Replace(file.Name(), ".lqs.txt", "", 1)
		fmt.Fprintln(w, fname)
	}
	return CONTINUE
}
