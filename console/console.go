package console

import (
	"fmt"
	"io"
	"math"
	"strings"

	"gitlab.com/carleeto/lq/macros"
	"gitlab.com/carleeto/lq/options"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/persistence"
	"gitlab.com/carleeto/lq/str_utils"
)

type ContinueOrNot int

const (
	STOP ContinueOrNot = iota
	CONTINUE
)

func is_reserved_alias(key string, cm []ConsoleCommand) bool {
	for _, c := range cm {
		for _, alias := range c.Aliases() {
			if key == alias {
				return true
			}
		}
	}
	return false
}

//aliases are shortforms
//mapping from alias to command name is one to one
func get_aliases_for(command string, cm []ConsoleCommand) (aliases []string) {
	for _, c := range cm {
		aliases = c.Aliases()
		for _, a := range aliases {
			if a == command {
				return aliases
			}
		}
	}
	return []string{}
}

func get_usage_for(command string, cm []ConsoleCommand) (usage string) {
	for _, c := range cm {
		aliases := c.Aliases()
		for _, a := range aliases {
			if a == command {
				return c.Usage()
			}
		}
	}
	return "Unknown command " + command
}

//macros are like #defines
//mapping from macros to a complete command with parameters is one to one
func get_macros(w io.Writer) (macros []string) {
	k := persistence.MacroKeyValStore(w)
	defer k.Close()
	return k.Keys()
}

func valid_commands(cm []ConsoleCommand, w io.Writer) []string {
	var commands []string
	for _, command := range cm {
		for _, alias := range command.Aliases() {
			commands = append(commands, alias)
		}
	}
	for _, command := range get_macros(w) {
		commands = append(commands, command)
	}
	return commands
}

func valid_command(c string, cm []ConsoleCommand, w io.Writer) bool {
	first_word, _ := str_utils.FirstWord(c)
	for _, command := range valid_commands(cm, w) {
		if command == first_word {
			return true
		}
	}
	return false
}

func remove_command(c string, cm []ConsoleCommand, w io.Writer) string {
	first_word, _ := str_utils.FirstWord(c)
	for _, alias := range valid_commands(cm, w) {
		if first_word == alias {
			return strings.TrimLeft(strings.Replace(c, alias, "", 1), " ")
		}
	}
	return c
}

func helpfunc(c string, w io.Writer) ContinueOrNot {
	if c == "" {
		fmt.Fprintf(w, "Valid commands are:\n")
		for _, command := range console_command_map() {
			print_aliases(command.Aliases(), w)
			fmt.Fprintf(w, ":\n\t")
			fmt.Fprintf(w, "%s\n", command.Usage())
		}
	} else {
		print_syntax_and_usage(c, "", console_command_map(), w)
	}
	return CONTINUE
}

func print_time_taken(c ConsoleCommand, w io.Writer) {
	fmt.Fprintln(w, c.TimeTakenForLastExecution())
}

func get_command_processor(c string, cm []ConsoleCommand, w io.Writer) ConsoleCommand {
	command_name, _ := str_utils.FirstWord(c)
	var f ConsoleCommand
	for _, command := range cm {
		for _, alias := range command.Aliases() {
			if alias == command_name {
				return command
			}
		}
	}
	if f == nil {
		fmt.Fprint(w, "How did you do that? (tell Carl, quick ;))\n")
	}
	return f
}

func get_closest_matching_commands(to string, cm []ConsoleCommand, w io.Writer) (closest_commands []string) {
	min_distance := math.MaxInt32
	d := 0
	for _, alias := range valid_commands(cm, w) {
		d = parsing.LevensteinDistance(alias, to)
		if d <= min_distance {
			min_distance = d
		}
	}
	for _, alias := range valid_commands(cm, w) {
		d2 := parsing.LevensteinDistance(alias, to)
		if min_distance == d2 {
			closest_commands = append(closest_commands, alias)
		}
	}
	return closest_commands
}

func print_key_val(key string, cm []ConsoleCommand, w io.Writer) {
	val := ""
	for _, console_function := range cm {
		for _, alias := range console_function.Aliases() {
			if alias == key {
				val = console_function.Usage()
			}
		}
	}
	if val == "" {
		k2 := persistence.MacroKeyValStore(w)
		val = k2.Get(key)
		defer k2.Close()
	}
	fmt.Fprintln(w, key+": "+val)
}

func print_list(items []string, cm []ConsoleCommand, w io.Writer) {
	for _, i := range items {
		print_key_val(i, cm, w)
	}
}

func print_possible_commands(command string, msg string, cm []ConsoleCommand, w io.Writer) {
	probably_meant := get_closest_matching_commands(command, cm, w)
	fmt.Fprintln(w, msg)
	print_list(probably_meant, cm, w)
}

func process_input(c string, cm []ConsoleCommand, log_time_taken bool, w io.Writer) ContinueOrNot {

	//will only work if the entire command is a macro
	if expanded_macro := macros.IsMacro(c, w); expanded_macro != "" {
		c = expanded_macro
	}
	if valid_command(c, cm, w) {
		f := get_command_processor(c, cm, w)
		args := remove_command(c, cm, w)
		ret := f.Execute(args, w)
		if log_time_taken {
			print_time_taken(f, w)
		}
		return ret
	} else {
		command, _ := str_utils.FirstWord(c)
		if strings.TrimSpace(command) != "" {
			fmt.Fprintf(w, "Unknown command "+c+"\n")
			print_possible_commands(command, "Did you mean...", cm, w)
		}
		return CONTINUE
	}
}

func StartConsole(history_store func() persistence.KVStore, r io.Reader, w io.Writer, log_time_taken bool) (options.Options, bool) {
	var o options.Options
	dummy_startup := func() {}
	cm := console_command_map()
	pi := func(c string) ContinueOrNot {
		return process_input(c, cm, log_time_taken, w)
	}
	f := func() persistence.KVStore {
		return persistence.HistoryKeyValStore(w)
	}
	do_console("\n(lq)", r, w, dummy_startup, pi, f)
	return o, false
}
