package console

import (
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/carleeto/lq/clipboard"
	"gitlab.com/carleeto/lq/execute"
	"gitlab.com/carleeto/lq/export"
	"gitlab.com/carleeto/lq/lqurl"
	"gitlab.com/carleeto/lq/os_utils"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/persistence"
	"gitlab.com/carleeto/lq/ssh"
	"gitlab.com/carleeto/lq/str_utils"
	"gitlab.com/carleeto/lq/version"
)

func store_key_val(fk func() persistence.KVStore, key, val string, usage func(persistence.KVStore), w io.Writer) ContinueOrNot {
	k := fk()
	if k.Error() != nil {
		fmt.Fprintln(w, k.Error())
		return CONTINUE
	}
	defer k.Close()

	if key == "" {
		usage(k)
		k.DisplayAll()
		return CONTINUE
	}
	k.Add(key, val)
	k.DisplayAll()
	return CONTINUE
}

func print_version(c string, w io.Writer) ContinueOrNot {
	fmt.Fprintln(w, "Version: "+version.Version)
	return CONTINUE
}

func script_console(c string, w io.Writer) ContinueOrNot {
	cm := scripting_command_map()
	if c == "" {
		print_syntax_and_usage("script", "<command>", console_command_map(), w)
		print_possible_commands("", "----------------------\nPossible commands are:\n----------------------", cm, w)
	}
	process_input(c, cm, false, w)
	return CONTINUE
}

func split_to_key_val(c string) (key string, val string) {
	key, _ = str_utils.FirstWord(c)
	val = strings.TrimLeft(strings.Replace(c, key, "", 1), " ")
	return key, val
}

func macro(c string, w io.Writer) ContinueOrNot {
	new_key, new_val := split_to_key_val(strings.Trim(c, " "))
	if is_reserved_alias(new_key, console_command_map()) {
		fmt.Fprintln(w, "Error: this is a reserved alias")
		return CONTINUE
	}
	usage := func(k persistence.KVStore) {
		print_syntax_and_usage("macro", "<name> <val>", console_command_map(), w)
		if len(k.Keys()) > 0 {
			fmt.Fprintln(w, "Defined macros:")
		}
	}
	f := func() persistence.KVStore {
		return persistence.MacroKeyValStore(w)
	}
	return store_key_val(f, new_key, new_val, usage, w)
}

func share(c string, w io.Writer) ContinueOrNot {
	args := get_flag_slice(w)
	params := parsing.ParsetoArguments(c)
	args = append(args, params...)
	lq_url := lqurl.ArgsToUrl(args)
	fmt.Fprintln(w, lq_url, "(copied to clipboard)")
	clipboard.CopyToClipboard(lq_url)
	return CONTINUE
}

func sh(c string, w io.Writer) ContinueOrNot {
	output, _ := os_utils.ExecuteShellCommand(c)
	fmt.Fprintln(w, string(output))
	return CONTINUE
}

func scp(c string, w io.Writer) ContinueOrNot {
	args := parsing.ParsetoArguments(c)
	if len(args) != 5 {
		fmt.Fprintln(w, "Syntax: scp <host> <user> <password> <remote_file> <local_file>")
	} else if len(args) == 5 {
		ssh.DoScp(args[0], args[1], args[2], args[3], args[4])
	}
	return CONTINUE
}

func ls(c string, w io.Writer) ContinueOrNot {
	if cwd, err := os_utils.CurrentDirectory(); err == nil {
		if files, err := os_utils.GetFilesIn(cwd); err == nil {
			for _, f := range files {
				fmt.Fprintln(w, f.Name())
			}
			fmt.Fprintf(w, "\n")
		} else {
			fmt.Fprintln(w, err)
			return CONTINUE
		}

	} else {
		fmt.Fprintln(w, err)
		return CONTINUE
	}
	return CONTINUE
}

func do(c string, w io.Writer) ContinueOrNot {
	return process_file_in_curdir_if(
		func(filename string) bool {
			for _, v := range strings.Split(c, " ") {
				if v != "" {
					lowercase_filename := strings.ToLower(filename)
					lowercase_substring := strings.ToLower(v)
					if strings.Contains(lowercase_filename, lowercase_substring) {
						return true
					}
				}
			}
			return false
		}, w)
}

func run(c string, w io.Writer) ContinueOrNot {
	cs := strings.Fields(c)
	if len(cs) == 0 {
		fmt.Fprintf(w, "Requires one or more arguments")
	} else {
		if err := execute.InterpretAndGo(cs, w); err != nil {
			fmt.Fprintln(w, err)
		}
	}
	return CONTINUE
}

func exprt(c string, w io.Writer) ContinueOrNot {
	cs := strings.Fields(c)
	if len(cs) < 2 {
		fmt.Fprintf(w, "What do you want to export?")
		print_syntax("export", "<flags|history|macros> <c:\\path\\to\\filename.json>", console_command_map(), w)
	} else {
		k := persistence.GetStoreByName(cs[0], w)
		if k.Error() != nil {
			fmt.Fprintln(w, k.Error())
			return CONTINUE
		}
		defer k.Close()
		m := k.KeyValMap()
		export.ExportToJSON(m, cs[1])
	}
	return CONTINUE
}

func exec(c string, w io.Writer) ContinueOrNot {
	s := get_flag_slice(w)
	cs := parsing.ParsetoArguments(c)
	for _, v := range cs {
		s = append(s, strings.TrimSpace(v))
	}
	if err := execute.InterpretAndGo(s, w); err != nil {
		fmt.Fprintln(w, err)
	}
	return CONTINUE
}

func doall(c string, w io.Writer) ContinueOrNot {
	return process_file_in_curdir_if(
		func(filename string) bool {
			return filename != ""
		}, w)
}

func history(c string, w io.Writer) ContinueOrNot {
	k := persistence.HistoryKeyValStore(w)
	if k.Error() != nil {
		fmt.Fprintln(w, k.Error())
		return CONTINUE
	}
	defer k.Close()
	fmt.Fprintf(w, format_key_vals(k, "%s: %s"))
	return CONTINUE
}

func get(c string, w io.Writer) ContinueOrNot {
	k := persistence.DefaultKeyValStore(w)
	if k.Error() != nil {
		fmt.Fprintln(w, k.Error())
		return CONTINUE
	}
	defer k.Close()
	new_key, new_val := split_to_key_val(c)
	if new_key == "" {
		print_syntax_and_usage("get", "<key>", console_command_map(), w)
	} else {
		fmt.Fprint(w, k.Get(new_key+new_val))
	}
	return CONTINUE
}

func nullfunc(string) ContinueOrNot {
	return CONTINUE
}

func reset(c string, w io.Writer) ContinueOrNot {
	args := parsing.ParsetoArguments(c)
	var k persistence.KVStore
	switch len(args) {
	case 0:
		print_syntax_and_usage("reset", "<flags|history|macros>", console_command_map(), w)
	case 1:
		store := args[0]
		k = persistence.GetStoreByName(store, w)
		if k.Error() != nil {
			fmt.Fprintln(w, k.Error())
			return CONTINUE
		}
		defer k.Close()
		k.Reset()
		fmt.Fprintf(w, "%s cleared.\n", store)
	}
	return CONTINUE
}

func set(c string, w io.Writer) ContinueOrNot {
	usage := func(persistence.KVStore) {
		print_syntax_and_usage("set", "<flag> <val>", console_command_map(), w)
	}
	new_key, new_val := split_to_key_val(strings.TrimLeft(c, " "))
	f := func() persistence.KVStore {
		return persistence.DefaultKeyValStore(w)
	}
	return store_key_val(f, new_key, new_val, usage, w)
}

func delete_entry(fk func() persistence.KVStore, key string, w io.Writer) (ContinueOrNot, error) {
	k := fk()
	if k.Error() != nil {
		fmt.Fprintln(w, k.Error())
		return CONTINUE, nil
	}
	defer k.Close()
	if key == "" {
		k.DisplayAll()
		print_syntax_and_usage("unset", "<flag>", console_command_map(), w)
	} else {
		if _, err := k.Remove(key); err != nil {
			return CONTINUE, fmt.Errorf("Unable to delete %s: %s", key, err)
		}
		k.DisplayAll()
	}
	return CONTINUE, nil
}

func unset(c string, w io.Writer) ContinueOrNot {
	key, _ := split_to_key_val(strings.Trim(c, " "))
	d := func() persistence.KVStore {
		return persistence.DefaultKeyValStore(w)
	}
	m := func() persistence.KVStore {
		return persistence.MacroKeyValStore(w)
	}
	if _, err := delete_entry(d, key, w); err != nil {
		if _, err := delete_entry(m, key, w); err != nil {
			fmt.Fprintln(w, err)
		}
	}
	return CONTINUE
}

func stopfunc(string, io.Writer) ContinueOrNot {
	return STOP
}

func format_key_vals(k persistence.KVStore, format string) string {
	k.GetAll()
	m := k.KeyValMap()
	is := []int{}

	for k, _ := range m {
		if i_, err := strconv.Atoi(k); err == nil {
			is = append(is, i_)
		}
	}

	sort.Sort(sort.IntSlice(is))

	str := ""
	for _, v := range is {
		sk := strconv.Itoa(v)
		str += fmt.Sprintf(format, sk, m[sk]) + "\n"
	}
	return str
}

func get_flag_slice(w io.Writer) []string {
	var s []string
	k := persistence.DefaultKeyValStore(w)
	if k.Error() != nil {
		fmt.Fprintln(w, k.Error())
		return s
	}
	defer k.Close()
	for k, v := range k.KeyValMap() {
		s = append(s, "-"+k)
		if v != "" {
			s = append(s, v)
		}
	}
	return s
}

func process_file_in_curdir_if(file_matches func(string) bool, w io.Writer) ContinueOrNot {
	cwd, err := os_utils.CurrentDirectory()
	if err != nil {
		fmt.Fprintln(w, err)
		return CONTINUE
	}
	files, err := os_utils.GetFilesIn(cwd)
	if err != nil {
		fmt.Fprintln(w, err)
		return CONTINUE
	}
	s := get_flag_slice(w)
	for _, f := range files {
		if file_matches(f.Name()) {
			fname := f.Name()
			execute.InterpretAndGo(append(s, fname), w)
		}
	}
	fmt.Fprintf(w, "\n")
	return CONTINUE
}

func print_aliases(aliases []string, w io.Writer) {
	fmt.Fprintf(w, "[")
	first_one := true
	for _, a := range aliases {
		if !first_one {
			fmt.Fprintf(w, "|")
		}
		first_one = false
		fmt.Fprintf(w, "%s", a)
	}
	fmt.Fprintf(w, "]")
}

func print_syntax(command string, parameters string, cm []ConsoleCommand, w io.Writer) {
	fmt.Fprintf(w, "Syntax: ")
	print_aliases(get_aliases_for(command, cm), w)
	fmt.Fprintf(w, " %s\n", parameters)
}

func print_syntax_and_usage(command string, parameters string, cm []ConsoleCommand, w io.Writer) {
	fmt.Fprintf(w, "%s\n", get_usage_for(command, cm))
	print_syntax(command, parameters, cm, w)
}
