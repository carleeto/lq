package console

import (
	"fmt"
	"gitlab.com/carleeto/lq/persistence"
	"os"
	"testing"
)

/*type KVStore interface {
	Close()
	Reset()
	Add(key, val string)
	Remove(key string) (wasdeleted bool, err error)
	DisplayAll()
	GetAll()
	Get(key string) string
	Error() error
	KeyValMap() map[string]string
	Keys() []string
}
*/

type dummy_keyval_store struct {
	m map[string]string
}

func (k *dummy_keyval_store) Close() {
}

func (k *dummy_keyval_store) Reset() {
}

func (k *dummy_keyval_store) Add(key, val string) {
	k.m[key] = val
}

func (k *dummy_keyval_store) Remove(key string) (wasdeleted bool, err error) {
	delete(k.m, key)
	return true, nil
}

func (k *dummy_keyval_store) DisplayAll() {
	for k, v := range k.m {
		fmt.Println(k, v)
	}
}

func (k *dummy_keyval_store) GetAll() {
}

func (k *dummy_keyval_store) Get(key string) string {
	return ""
}

func (k dummy_keyval_store) Error() error {
	return nil
}

func (k dummy_keyval_store) KeyValMap() (key_val_map map[string]string) {
	return k.m
}

func (k dummy_keyval_store) Keys() (keys []string) {
	k.GetAll()
	for k, _ := range k.m {
		keys = append(keys, k)
	}
	return keys
}

func TestUnsetExistingFlag(t *testing.T) {
	k := new(dummy_keyval_store)
	k.m = make(map[string]string)
	k.Add("sip", "")
	fk := func() persistence.KVStore {
		return k
	}
	delete_entry(fk, "sip", os.Stdout)
	if len(k.m) > 0 {
		t.Fail()
	}
}

func TestUnsetNonExistentFlagReturnsContinue(t *testing.T) {
	k := new(dummy_keyval_store)
	k.m = make(map[string]string)
	fk := func() persistence.KVStore {
		return k
	}
	if c, _ := delete_entry(fk, "sip", os.Stdout); c != CONTINUE {
		t.Fatalf("delete_entry must return CONTINUE even if the flag to be deleted does not exist")
	}
}

func TestUnsetNonExistentFlagDoesNotReturnAnError(t *testing.T) {
	k := new(dummy_keyval_store)
	k.m = make(map[string]string)
	fk := func() persistence.KVStore {
		return k
	}
	if _, err := delete_entry(fk, "sip", os.Stdout); err != nil {
		t.Fatalf("delete_entry returned an error while trying to delete a non-existent flag. %s", err)
	}
}
