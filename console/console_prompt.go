package console

import (
	"bufio"
	"fmt"
	"io"
	"gitlab.com/carleeto/lq/persistence"
)

func get_line(s *bufio.Scanner) string {
	return s.Text()
}

func next_index_for_store(h persistence.KVStore) int {
	h.GetAll()
	return len(h.Keys()) + 1
}

func store_in_history(h persistence.KVStore, line string) {
	if h != nil {
		command_num := next_index_for_store(h)
		key := fmt.Sprintf("%d", command_num)
		h.Add(key, line)
	}
}

func do_console(prompt string, r io.Reader, w io.Writer, startup func(), input_processor func(string) ContinueOrNot, history_store func() persistence.KVStore) {
	scanner := bufio.NewScanner(r)
	startup()
	fmt.Fprintf(w, prompt)
	for scanner.Scan() {
		line := get_line(scanner)
		if history_store != nil {
			if h := history_store(); h != nil {
				store_in_history(h, line)
				h.Close()
			}
		}
		if input_processor(line) == STOP {
			break
		}
		fmt.Fprintf(w, prompt)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(w, "reading standard input:", err)
	}
}
