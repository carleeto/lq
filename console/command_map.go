package console

import (
	"io"
	"time"
)

type ConsoleCommand interface {
	Execute(string, io.Writer) ContinueOrNot
	TimeTakenForLastExecution() time.Duration
	Usage() string
	Aliases() []string
}

type CommandMap interface {
	Functions() []ConsoleCommand
}
