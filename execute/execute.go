package execute

import (
	//	"gitlab.com/carleeto/lq/console"

	"fmt"
	"io"
	"log"
	"os"
	"runtime/pprof"

	"gitlab.com/carleeto/lq/options"
	"gitlab.com/carleeto/lq/pipeline"
)

func preprocess(o options.Options, w io.Writer) (postprocess func()) {
	if o.Cpuprofile != "" {
		fmt.Fprintln(w, "start profiling")
		cpudata, err := os.Create(o.Cpuprofile + ".cpu.dat")
		if err != nil {
			log.Fatal(err)
		}
		memdata, err := os.Create(o.Cpuprofile + ".memory.dat")
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(cpudata)
		return func() {
			fmt.Fprintln(w, "stop profiling")
			pprof.StopCPUProfile()
			pprof.WriteHeapProfile(memdata)
		}
	}
	return func() {}
}

func Process(o options.Options, w io.Writer) {
	//setup the profiler, for example, if necessary
	postprocess := preprocess(o, w)
	//run the pipeline
	pipeline.StartProcessing(o, w)
	//any teardown
	postprocess()
}

func InterpretAndGo(args []string, w io.Writer) error {
	if o, process, _, err := options.Interpret(args, w); process && err == nil {
		Process(o, w)
		return nil
	} else {
		return err
	}
}
