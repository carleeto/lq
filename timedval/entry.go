package timedval

import (
	"time"

	"gitlab.com/carleeto/lq/context"
	"gitlab.com/carleeto/lq/str_utils"
)

type Entry struct {
	Timestamp        time.Time
	Contexts         []context.Context
	Data             []byte
	timestamp_format string
	hidden           bool
	show_timestamp   bool
	show_contexts    bool
}

func (e Entry) GetData() []byte {
	return e.Data
}

func (e *Entry) SetData(data []byte) {
	e.Data = data
}

func (e Entry) RenderOptions() (show_timestamp, show_contexts bool) {
	return e.show_timestamp, e.show_contexts
}

func (e *Entry) ShowTimestamp(show_timestamp bool) {
	e.show_timestamp = show_timestamp
}

func (e *Entry) ShowContexts(show_contexts bool) {
	e.show_contexts = show_contexts
}

func (e Entry) GetContexts() []context.Context {
	return e.Contexts
}

func (e *Entry) SetContexts(contexts []context.Context) {
	e.Contexts = contexts
}

func (e Entry) GetTimestamp() time.Time {
	return e.Timestamp
}

func (e *Entry) SetTimestamp(t time.Time) {
	e.Timestamp = t
}

func (e *Entry) Hide() {
	e.hidden = true
}

func (e *Entry) IsHidden() bool {
	return e.hidden
}

func (e *Entry) SetTimestampFormat(f string) {
	e.timestamp_format = f
}

func (e Entry) GetTimestampFormat() string {
	return e.timestamp_format
}

func (e Entry) String() string {
	var s string
	show_timestamp, show_contexts := e.RenderOptions()
	if show_timestamp {
		s = e.Timestamp.Format(e.timestamp_format)
	}
	if show_contexts {
		s = str_utils.AppendStrings(s, " ", 0)
		for _, i := range e.Contexts {
			s = str_utils.AppendStrings(s, i.String(), 0)
		}
	}
	s = str_utils.AppendStrings(s, string(e.Data), 0)
	return s
}

func (e *Entry) Release() {
	e.Contexts = nil
	e.Data = nil
}
