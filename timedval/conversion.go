package timedval

import (
	"gitlab.com/carleeto/lq/context"
	"gitlab.com/carleeto/lq/parsing"
	"gitlab.com/carleeto/lq/str_utils"
)

func new_entry(line string) *Entry {
	e := new(Entry)
	e.Timestamp, e.timestamp_format = parsing.GetTimestamp(line)
	c, s := context.GetContext(line)
	e.Contexts = c
	e.Data = str_utils.StringToByteSlice(s)
	e.show_timestamp = true
	e.show_contexts = true
	return e
}

func append_to_entry(line string, e *Entry) *Entry {
	e.Data = append(e.Data, str_utils.StringToByteSlice(line)...)
	return e
}

func ConvertLineToTimedVal(cin <-chan string) <-chan TimedVal {
	cout := make(chan TimedVal)
	go func() {
		current_entry := new(Entry)
		for line := range cin {
			if parsing.StartsWithTimestamp(line) {
				cout <- current_entry
				current_entry = new_entry(line)
			} else {
				current_entry = append_to_entry(line, current_entry)
			}
		}
		cout <- current_entry
		close(cout)
	}()
	return cout
}
