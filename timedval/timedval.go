package timedval

import (
	"time"

	"gitlab.com/carleeto/lq/context"
)

type TimedVal interface {
	GetTimestamp() time.Time
	SetTimestamp(t time.Time)
	GetContexts() []context.Context
	SetContexts(contexts []context.Context)
	GetData() []byte
	SetData([]byte)
	String() string
	Hide()
	IsHidden() bool
	SetTimestampFormat(string)
	GetTimestampFormat() string
	ShowTimestamp(bool)
	ShowContexts(bool)
	RenderOptions() (show_timestamp, show_contexts bool)
	Release()
}
