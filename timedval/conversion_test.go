package timedval

import (
	"bytes"
	"testing"

	"gitlab.com/carleeto/lq/str_utils"
)

func testline() string {
	return "17:00:08.911             : {10000} : [611¤¤SERVERMANAGER¤NZVOIP04¤SR¤®SE¤OK [sent:6, queued:0, discarded:0]¤®¶]                   : sent to {10080}"
}

func expected_data() string {
	line := testline()
	return line[13:len(line)]
}

func TestEntryIsCreated(t *testing.T) {
	line := testline()
	e := new_entry(line)
	if e == nil {
		t.Fatal("New entry not created")
	}
}

func TestEntryHasData(t *testing.T) {
	line := testline()
	e := new_entry(line)
	if e.Data == nil {
		t.Fatal("New entry does not contain data")
	}
}

func TestEntryDataIsCorrect(t *testing.T) {
	line := testline()
	e := new_entry(line)
	if bytes.Compare(e.Data, str_utils.StringToByteSlice(expected_data())) != 0 {
		t.Fatal("New entry does not contain data")
	}
}

func TestDontCrashOnTimeStampLine2(t *testing.T) {
	c := make(chan string)
	defer close(c)
	f := ConvertLineToTimedVal(c)
	c <- "15:00:01.023"
	println((<-f).GetData())
}

func TestDontCrashOnTimeStampLine(t *testing.T) {
	s := "15:00:01.023"
	println(new_entry(s).Data)
}
