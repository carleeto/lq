package timedval

import "time"

func AddDate(date_in <-chan time.Time, c <-chan TimedVal) <-chan TimedVal {
	cout := make(chan TimedVal)
	go func() {
		d := <-date_in
		for e := range c {
			if e != nil {
				t := e.GetTimestamp()
				if t.Year() == 0 {
					t_new := time.Date(
						d.Year(), d.Month(), d.Day(),
						t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), t.Location())
					e.SetTimestamp(t_new)
				}
			}
			cout <- e
		}
		close(cout)
	}()
	return cout
}

func TSAdjust(d time.Duration, cin <-chan TimedVal) <-chan TimedVal {
	if d == 0 {
		return cin
	}
	cout := make(chan TimedVal)
	go func() {
		for e := range cin {
			if e != nil {
				e.SetTimestamp(e.GetTimestamp().Add(d))
			}
			cout <- e
		}
		close(cout)
	}()
	return cout
}
