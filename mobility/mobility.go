package mobility

import (
	"gitlab.com/carleeto/lq/context"
	"gitlab.com/carleeto/lq/timedval"
)

type MergeState struct {
	memory timedval.TimedVal
}

func (m *MergeState) Merge(cin <-chan timedval.TimedVal) <-chan timedval.TimedVal {
	cout := make(chan timedval.TimedVal)
	go func() {
		for e := range cin {
			if e != nil {
				if m.memory != nil {
					if context.ContextsAreEqual(e.GetContexts(), m.memory.GetContexts()) {
						d_ := e.GetData()
						d := m.memory.GetData()
						d = append(d, d_...)
						m.memory.SetData(d)
					} else {
						cout <- m.memory
						m.memory = e
					}
				} else {
					m.memory = e
				}
			}
		}
		if m.memory != nil {
			cout <- m.memory
		}
		close(cout)
	}()
	return cout
}
